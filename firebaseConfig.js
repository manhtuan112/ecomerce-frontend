// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import {getStorage} from 'firebase/storage'

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyC2WpMPcM6t2YbVucY8cuG3MUDIxU4bQUk",
  authDomain: "ecommerce-image-a4ac6.firebaseapp.com",
  projectId: "ecommerce-image-a4ac6",
  storageBucket: "ecommerce-image-a4ac6.appspot.com",
  messagingSenderId: "70658258945",
  appId: "1:70658258945:web:a8fe7230b462d3bce9a800"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const storage = getStorage(app)