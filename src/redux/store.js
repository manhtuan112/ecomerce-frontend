import { configureStore } from '@reduxjs/toolkit';
import userReducer from '../features/authenticate/userSlice'
import productSlice from '../features/product/productSlice';
import reviewSlice from '../features/review/reivewSlice';
import cartSlice from '../features/cart/cartSlice';
import orderSlice from '../features/order/orderSlice';
import shipmentSlice from '../features/shipment/shipmentSlice';
import chatbotSlice from '../features/chatbot/chatbotSlice';

export const store = configureStore({
  reducer:{
    user: userReducer,
    product: productSlice,
    review: reviewSlice,
    cart: cartSlice,
    order: orderSlice,
    shipment: shipmentSlice,
    chatbot: chatbotSlice, 

  }
});