import React from 'react'
import { Outlet } from 'react-router-dom'
import Header from '../components/header'
import Footer from '../components/footer'
import Chatbot from '../components/chat_bot'

function MainLayout() {
  return (
    <>
      {/* <Chatbot /> */}
      <Header />
      <Outlet />
      <Footer />
    </>
  )
}

export default MainLayout
