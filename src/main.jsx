import React from 'react'
import ReactDOM from 'react-dom/client'
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App.jsx'
import { BrowserRouter as Router } from 'react-router-dom'
import { store } from './redux/store.js'
import { Provider } from 'react-redux'
import ScrollToTop from './features/ScrollToTop.js'

ReactDOM.createRoot(document.getElementById('root')).render(
  <Provider store={store}>
    <Router>
      <ScrollToTop />
      <App />
    </Router>
  </Provider>
)
