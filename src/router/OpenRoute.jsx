import React from 'react'
import { Navigate, Outlet } from 'react-router-dom'
import { getLocalStorage, setLocalStorage } from '../utils/localStorage'

const OpenRoute = ({children}) => {
  const token = getLocalStorage('accessToken')
  return !token ? children : <Navigate to='/' replace={true} />
}

export default OpenRoute
