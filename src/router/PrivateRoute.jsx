import React from 'react'
import { Navigate, Outlet } from 'react-router-dom'
import { getLocalStorage, setLocalStorage } from '../utils/localStorage'

const PrivateRoute = ({children}) => {
  const token = getLocalStorage('accessToken')
  return token ? children : <Navigate to='/login' replace={true} />
}

export default PrivateRoute
