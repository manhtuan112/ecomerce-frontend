import axios from 'axios';
import queryString from 'query-string'

export const createAxiosInstance = (baseURL) => {
  const instance = axios.create({
    withCredentials: false,
    baseURL: baseURL,
    timeout: 5000,
    headers: {
      "Content-type": "application/json",
    },
    paramsSerializer: (params) => queryString.stringify(params)
  });

  instance.interceptors.request.use((request) => {
    // Check if the request is for logging in
    const isLoginRequest = request.url.includes('/user/auth/login'); // Modify the condition based on your login endpoint
    const isSignupRequest = request.url.includes('/user/auth/signup'); // Modify the condition based on your login endpoint
    const isForgotPasswordRequest = request.url.includes('/user/forget_password'); // Modify the condition based on your login endpoint
    const isVerifyOtpRequest = request.url.includes('user/verify_otp'); // Modify the condition based on your login endpoint

    const isChangePasswordWithOTPRequest = request.url.includes('user/change_password_with_otp'); // Modify the condition based on your login endpoint


    // Add Authorization header only for non-login requests
    if (!isLoginRequest && !isSignupRequest && !isForgotPasswordRequest && !isVerifyOtpRequest && !isChangePasswordWithOTPRequest) {
      const accessToken = localStorage.getItem("accessToken");
      const accessHeader = `Bearer ${accessToken}`;
      if (request.headers) request.headers['Authorization'] = accessHeader;
    }

    // check API chat GPT to get token
    if (request.baseURL.includes('api.openai.com/v1')) {
      const token = import.meta.env.VITE_CHAT_API_KEY || process.env.REACT_APP_API_TOKEN;
      const accessHeader = `Bearer ${token}`;
      if (request.headers) request.headers['Authorization'] = accessHeader;
    }


    return request;
  });

  return instance;
};