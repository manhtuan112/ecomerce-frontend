export const service = [
  {
    "title": "Free Shipping",
    "tagline": "Form all orders orver 120.000 VND",
    "icon": "LiaShippingFastSolid",
  },
  {
    "title": "Daily Surprise Offers",
    "tagline": "Save upto 25% off",
    "icon": "GoGift",
  },
  {
    "title": "Support 24/7",
    "tagline": "Shop with an expert",
    "icon": "FaHeadset",
  },
  {
    "title": "Affordable Prices",
    "tagline": "Get Factory Default Price",
    "icon": "TbDiscount2",
  },
  {
    "title": "Secure Payments",
    "tagline": "100% Protected Payment",
    "icon": "MdPayments",
  },

]
