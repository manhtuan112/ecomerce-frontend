import { createSlice } from '@reduxjs/toolkit'
import { setLocalStorage } from '../../../utils/localStorage'
import { getDetailProduct, getListBrandMethod, getListCategoryMethod, getListCollectionMethod, getListProductMethod, getProductInfoByCartMethod, updateRatingStartForProductMethod } from '../middleware'

const statusType = {
  isLoading: false,
  isSuccess: false,
  isError: false
}

const initialState = {
  status: statusType,
  list_product: null,
  list_category: null,
  list_colection: null,
  list_brand: null,
  list_product_by_colection: null,
  current_product: null, //in detail page
}

export const productSlice = createSlice({
  name: 'product',
  initialState,
  reducers: {
  },
  extraReducers: (builder) => {
    builder
      // Get List Product
      .addCase(getListProductMethod.pending, (state, action) => {
        state.status.isLoading = true
        state.status.isError = false
        state.status.isSuccess = false
      })
      .addCase(getListProductMethod.fulfilled, (state, action) => {
        state.list_product = action.payload.data
        state.status.isLoading = false
        state.status.isError = false
        state.status.isSuccess = true
      })
      .addCase(getListProductMethod.rejected, (state, action) => {
        state.status.isLoading = false
        state.status.isError = true
        state.status.isSuccess = false
      })

      // Get List Product
      .addCase(getDetailProduct.pending, (state, action) => {
        state.status.isLoading = true
        state.status.isError = false
        state.status.isSuccess = false
      })
      .addCase(getDetailProduct.fulfilled, (state, action) => {
        state.current_product = action.payload.data
        state.status.isLoading = false
        state.status.isError = false
        state.status.isSuccess = true
      })
      .addCase(getDetailProduct.rejected, (state, action) => {
        state.status.isLoading = false
        state.status.isError = true
        state.status.isSuccess = false
      })

      // Get List Category
      .addCase(getListCategoryMethod.pending, (state, action) => {
        state.status.isLoading = true
        state.status.isError = false
        state.status.isSuccess = false
      })
      .addCase(getListCategoryMethod.fulfilled, (state, action) => {
        state.list_category = action.payload.data
        state.status.isLoading = false
        state.status.isError = false
        state.status.isSuccess = true
      })
      .addCase(getListCategoryMethod.rejected, (state, action) => {
        state.status.isLoading = false
        state.status.isError = true
        state.status.isSuccess = false
      })

      // Get List Brand
      .addCase(getListBrandMethod.pending, (state, action) => {
        state.status.isLoading = true
        state.status.isError = false
        state.status.isSuccess = false
      })
      .addCase(getListBrandMethod.fulfilled, (state, action) => {
        state.list_brand = action.payload.data
        state.status.isLoading = false
        state.status.isError = false
        state.status.isSuccess = true
      })
      .addCase(getListBrandMethod.rejected, (state, action) => {
        state.status.isLoading = false
        state.status.isError = true
        state.status.isSuccess = false
      })

      // Get List Collection
      .addCase(getListCollectionMethod.pending, (state, action) => {
        state.status.isLoading = true
        state.status.isError = false
        state.status.isSuccess = false
      })
      .addCase(getListCollectionMethod.fulfilled, (state, action) => {
        state.list_colection = action.payload.data
        state.status.isLoading = false
        state.status.isError = false
        state.status.isSuccess = true
      })
      .addCase(getListCollectionMethod.rejected, (state, action) => {
        state.status.isLoading = false
        state.status.isError = true
        state.status.isSuccess = false
      })

      // Update Rating Star
      .addCase(updateRatingStartForProductMethod.pending, (state, action) => {
        state.status.isLoading = true
        state.status.isError = false
        state.status.isSuccess = false
      })
      .addCase(updateRatingStartForProductMethod.fulfilled, (state, action) => {
        state.status.isLoading = false
        state.status.isError = false
        state.status.isSuccess = true
      })
      .addCase(updateRatingStartForProductMethod.rejected, (state, action) => {
        state.status.isLoading = false
        state.status.isError = true
        state.status.isSuccess = false
      })

      // Get Product By Cart
      .addCase(getProductInfoByCartMethod.pending, (state, action) => {
        state.status.isLoading = true
        state.status.isError = false
        state.status.isSuccess = false
      })
      .addCase(getProductInfoByCartMethod.fulfilled, (state, action) => {
        state.status.isLoading = false
        state.status.isError = false
        state.status.isSuccess = true
      })
      .addCase(getProductInfoByCartMethod.rejected, (state, action) => {
        state.status.isLoading = false
        state.status.isError = true
        state.status.isSuccess = false
      })

  }
})

export const { } = productSlice.actions
export default productSlice.reducer