import { createAxiosInstance } from '../../../utils/axiosInstance'

export const ProductApiMethod = {
  baseUrl: import.meta.env.VITE_API_URL_PRODUCT || process.env.REACT_APP_API_URL,
  getListProduct: () => {
    const axiosInstance = createAxiosInstance(ProductApiMethod.baseUrl)
    return axiosInstance.get('/product/get_all/')
  },

  getListCategory: () => {
    const axiosInstance = createAxiosInstance(ProductApiMethod.baseUrl)
    return axiosInstance.get('/category/get_all/')
  },

  getListCollection: () => {
    const axiosInstance = createAxiosInstance(ProductApiMethod.baseUrl)
    return axiosInstance.get('/colection/get_all/')
  },

  getListBrand: () => {
    const axiosInstance = createAxiosInstance(ProductApiMethod.baseUrl)
    return axiosInstance.get('/brand/get_all/')
  },

  getDetailProduct: (data) => {
    const axiosInstance = createAxiosInstance(ProductApiMethod.baseUrl)
    return axiosInstance.get(`/product/get_detail_by_id/${data}/`)
  },

  updateRatingStartForProduct: (data, id) => {
    const axiosInstance = createAxiosInstance(ProductApiMethod.baseUrl)
    return axiosInstance.put(`/product/update_product_by_id/${id}/`, data)
  },

  getProductByCart: (data) => {
    const axiosInstance = createAxiosInstance(ProductApiMethod.baseUrl)
    return axiosInstance.get(`/product/get_by_cart/${data}/`)
  },



}