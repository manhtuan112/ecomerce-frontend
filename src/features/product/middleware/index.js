import { createAsyncThunk } from '@reduxjs/toolkit';
import { ProductApiMethod } from '../api';

export const getListProductMethod = createAsyncThunk(
  'product/getListProductMethod',
  async (data, thunkAPI) => {
    try {
      const response = await ProductApiMethod.getListProduct();
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const getListCategoryMethod = createAsyncThunk(
  'category/getListCategoryMethod',
  async (data, thunkAPI) => {
    try {
      const response = await ProductApiMethod.getListCategory();
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const getListCollectionMethod = createAsyncThunk(
  'collection/getListCategoryMethod',
  async (data, thunkAPI) => {
    try {
      const response = await ProductApiMethod.getListCollection();
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const getListBrandMethod = createAsyncThunk(
  'brand/getListBrandMethod',
  async (data, thunkAPI) => {
    try {
      const response = await ProductApiMethod.getListBrand();
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);


export const getDetailProduct = createAsyncThunk(
  'product/getDetailProduct',
  async (data, thunkAPI) => {
    try {
      const response = await ProductApiMethod.getDetailProduct(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const updateRatingStartForProductMethod = createAsyncThunk(
  'product/updateReiview',
  async ({data, id},  thunkAPI) => {
    try {
      const response = await ProductApiMethod.updateRatingStartForProduct(data, id);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }
  }
);


export const getProductInfoByCartMethod = createAsyncThunk(
  'product/getProductInfoByCart',
  async (data, thunkAPI) => {
    try {
      const response = await ProductApiMethod.getProductByCart(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);