import { createSlice } from '@reduxjs/toolkit'
import { addShipmentAddressMethod, createShipmentMethod, getShipmentAddressListMethod } from '../middleware'
import { setLocalStorage } from '../../../utils/localStorage'

const statusType = {
  isLoading: false,
  isSuccess: false,
  isError: false
}

const shipmentInfoType = {
  free: 0,
  shipment_date: '',
  shipment_address: '',
}

const initialState = {
  status: statusType,
  shipmentInfo: shipmentInfoType
}

export const shipmentSlice = createSlice({
  name: 'shipment',
  initialState,
  reducers: {
    setShipmentInfo: (state, action) => {
      state.shipmentInfo = action.payload
      console.log(action.payload)
      localStorage.setItem('shipmentInfo', JSON.stringify(action.payload))
    },
  },
  extraReducers: (builder) => {
    builder
      // Get List Shipment Address
      .addCase(getShipmentAddressListMethod.pending, (state, action) => {
        state.status.isLoading = true
        state.status.isError = false
        state.status.isSuccess = false
      })
      .addCase(getShipmentAddressListMethod.fulfilled, (state, action) => {
        state.status.isLoading = false
        state.status.isError = false
        state.status.isSuccess = true
      })
      .addCase(getShipmentAddressListMethod.rejected, (state, action) => {
        state.status.isLoading = false
        state.status.isError = true
        state.status.isSuccess = false
      })

      // Add Shipment Address
      .addCase(addShipmentAddressMethod.pending, (state, action) => {
        state.status.isLoading = true
        state.status.isError = false
        state.status.isSuccess = false
      })
      .addCase(addShipmentAddressMethod.fulfilled, (state, action) => {
        state.status.isLoading = false
        state.status.isError = false
        state.status.isSuccess = true
      })
      .addCase(addShipmentAddressMethod.rejected, (state, action) => {
        state.status.isLoading = false
        state.status.isError = true
        state.status.isSuccess = false
      })

      // Create Shipment
      .addCase(createShipmentMethod.pending, (state, action) => {
        state.status.isLoading = true
        state.status.isError = false
        state.status.isSuccess = false
      })
      .addCase(createShipmentMethod.fulfilled, (state, action) => {
        state.status.isLoading = false
        state.status.isError = false
        state.status.isSuccess = true
      })
      .addCase(createShipmentMethod.rejected, (state, action) => {
        state.status.isLoading = false
        state.status.isError = true
        state.status.isSuccess = false
      })


  }
})

export const { setShipmentInfo } = shipmentSlice.actions
export default shipmentSlice.reducer