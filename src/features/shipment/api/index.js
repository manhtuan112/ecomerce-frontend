import { createAxiosInstance } from '../../../utils/axiosInstance'

export const ShipmentApiMethod = {
  baseUrl: import.meta.env.VITE_API_URL_SHIPMENT || process.env.REACT_APP_API_URL,


  getShipmentAddressList: (data) => {
    const axiosInstance = createAxiosInstance(ShipmentApiMethod.baseUrl)
    return axiosInstance.get(`/shipment/get_by_username/${data}/`)
  },

  addShipmentAddress: (data) => {
    const axiosInstance = createAxiosInstance(ShipmentApiMethod.baseUrl)
    return axiosInstance.post(`/shipment/add_new_shipment_address/`, data)
  },

  createShipment: (data) => {
    const axiosInstance = createAxiosInstance(ShipmentApiMethod.baseUrl)
    return axiosInstance.post(`/shipment/create_shipment/`, data)
  },



}