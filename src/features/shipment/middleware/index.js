import { createAsyncThunk } from '@reduxjs/toolkit';
import { ShipmentApiMethod } from '../api';


export const getShipmentAddressListMethod = createAsyncThunk(
  'shipment/getShipmentAddressListMethod',
  async (data, thunkAPI) => {
    try {
      const response = await ShipmentApiMethod.getShipmentAddressList(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const addShipmentAddressMethod = createAsyncThunk(
  'shipment/addShipmentAddressMethod',
  async (data, thunkAPI) => {
    try {
      const response = await ShipmentApiMethod.addShipmentAddress(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);


export const createShipmentMethod = createAsyncThunk(
  'shipment/createShipmentMethod',
  async (data, thunkAPI) => {
    try {
      const response = await ShipmentApiMethod.createShipment(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);