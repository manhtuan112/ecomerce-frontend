const removeDescription = (str, max_length) => {
  if (str.length > max_length) {
    str = str.substring(0, max_length) + '...'
  }
  return str
}

const convertDate = (dateStr, isBirthday) => {
  if (isBirthday) {
    let dateParts = dateStr.split("-");
    return dateParts[2] + "/" + dateParts[1] + "/" + dateParts[0]
  }
  const date = new Date(dateStr)

  const day = ('0' + date.getDate()).slice(-2)
  const month = ('0' + (date.getMonth() + 1)).slice(-2)
  const year = date.getFullYear()
  return day + '/' + month + '/' + year
}

const getCurrentDateTimeFormatted = () => {
  const now = new Date();

  // Get day, month, and year components
  const day = String(now.getDate()).padStart(2, '0');
  const month = String(now.getMonth() + 1).padStart(2, '0');
  const year = String(now.getFullYear());

  // Get seconds, minutes, and hours components
  const seconds = String(now.getSeconds()).padStart(2, '0');
  const minutes = String(now.getMinutes()).padStart(2, '0');
  const hours = String(now.getHours()).padStart(2, '0');

  // Concatenate the components in the desired format
  const formattedDateTime = day + month + year + seconds + minutes + hours;

  return formattedDateTime;
}

const convertMoneyFormat = (money) => {
  let atmp = String(money);
  let res = "";
  let tmp = "";
  let index = 0;
  for (let i = atmp.length - 1; i >= 0; i--) {
    tmp = atmp.charAt(i) + tmp;
    index += 1;
    if (index === 3) {
      res = "." + tmp + res;
      tmp = "";
      index = 0;
    }
  }
  res = tmp + res;
  if (res.charAt(0) === ".") {
    res = res.substring(1);
  }
  return res;
}

const convertDateTime = (value) => { //convert 2023-12-15T13:20:02.701593Z => 13:20 15/12/2023
  const date = new Date(value);
  let hour = date.getUTCHours()
  if (hour < 10) {
    hour = '0' + hour
  }
  let minute = date.getMinutes();
  if (minute < 10) {
    minute = '0' + minute
  }


  const formattedDate = `${hour}:${minute} ${date.getUTCDate()}-${date.getUTCMonth() + 1}-${date.getUTCFullYear()}`;
  return formattedDate
}

convertDate('2023-12-21T03:24:54.044751Z')

export { removeDescription, convertDate, getCurrentDateTimeFormatted, convertMoneyFormat, convertDateTime }