import { createAxiosInstance } from '../../../utils/axiosInstance'

export const AuthApiMethod = {
  baseUrl: import.meta.env.VITE_API_URL || process.env.REACT_APP_API_URL,
  login: (data) => {
    const axiosInstance = createAxiosInstance(AuthApiMethod.baseUrl)
    return axiosInstance.post('/user/auth/login/', data)
  },

  signup: (data) => {
    const axiosInstance = createAxiosInstance(AuthApiMethod.baseUrl)
    return axiosInstance.post('/user/auth/signup/', data)
  },

  getGeneralUserInfo: () => {
    const axiosInstance = createAxiosInstance(AuthApiMethod.baseUrl)
    return axiosInstance.get('/user/general_user_info/')
  },

  getProfile: () => {
    const axiosInstance = createAxiosInstance(AuthApiMethod.baseUrl)
    return axiosInstance.get('/user/profile/')
  },

  changeProfile: (data) => {
    const axiosInstance = createAxiosInstance(AuthApiMethod.baseUrl)
    return axiosInstance.put('/user/update/', data)
  },

  changePassword: (data) => {
    const axiosInstance = createAxiosInstance(AuthApiMethod.baseUrl)
    return axiosInstance.put('/user/change_password/', data)
  },

  forgetPassword: (data) => {
    const axiosInstance = createAxiosInstance(AuthApiMethod.baseUrl)
    return axiosInstance.post('/user/forget_password/', data)
  },

  verifyOtp: (data) => {
    const axiosInstance = createAxiosInstance(AuthApiMethod.baseUrl)
    return axiosInstance.post('/user/verify_otp/', data)
  },

  changePasswordWithOtp: (data) => {
    const axiosInstance = createAxiosInstance(AuthApiMethod.baseUrl)
    return axiosInstance.post('/user/change_password_with_otp/', data)
  }

}