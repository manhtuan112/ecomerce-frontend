import { createSlice } from '@reduxjs/toolkit'
import { changePassword, changePasswordWithOtpMethod, changeUserInfoMethod, forgotPassword, getGeneralUserInfoMethod, getUserInfoMethod, loginMethod, signupMethod, verifyOtp } from '../middleware'
import { setLocalStorage } from '../../../utils/localStorage'

const statusType = {
  isLoading: false,
  isSuccess: false,
  isError: false
}

const initialState = {
  username: '',
  isLogin: "false",
  avatar: null,
  isSignup: 'false',
  failedMessage: '',
  status: statusType,
}

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setLogin: (state, action) => {
      state.isLogin = action.payload.isLogin
    },
    setSignup: (state, action) => {
      state.isSignup = action.payload.isSignup
    },
    resetState: (state, action) => {
      return {
        username: '',
        isLogin: 'false',
        avatar: null,
        isSignup: 'false',
        failedMessage: '',
        status: statusType,
      }
    },
    setLoading: (state, action) => {
      state.status.isLoading = true
    },
    setGeneralUserInfo: (state, action) => {
      state.avatar = action.payload.avatar
      state.username = action.payload.username
    },
  },
  extraReducers: (builder) => {
    builder
      // Login
      .addCase(loginMethod.pending, (state, action) => {
        state.isLogin = 'pending'
        state.failedMessage = null
      })
      .addCase(loginMethod.fulfilled, (state, action) => {
        state.isLogin = 'success'
        state.username = action.payload.data.username
        state.avatar = action.payload.data.avatar
        state.failedMessage = null
        setLocalStorage('accessToken', action.payload.tokens.access);
      })
      .addCase(loginMethod.rejected, (state, action) => {
        state.isLogin = 'failed'
        state.failedMessage = action.payload
      })

      // Sign up
      .addCase(signupMethod.pending, (state, action) => {
        state.isSignup = 'pending'
        state.failedMessage = null
      })
      .addCase(signupMethod.fulfilled, (state, action) => {
        state.isSignup = 'success'
      })
      .addCase(signupMethod.rejected, (state, action) => {
        state.isSignup = 'failed'
        const fisrtKey = Object.keys(action.payload)[0]
        state.failedMessage = action.payload[fisrtKey]
      })

      // Get General User Info
      .addCase(getGeneralUserInfoMethod.pending, (state, action) => {
        state.isLogin = 'pending'
      })
      .addCase(getGeneralUserInfoMethod.fulfilled, (state, action) => {
        state.username = action.payload.data.username
        state.avatar = action.payload.data.avatar
        state.isLogin = 'success'
      })
      .addCase(getGeneralUserInfoMethod.rejected, (state, action) => {
        return {
          username: '',
          isLogin: 'false',
          avatar: null,
          isSignup: 'false',
          failedMessage: '',
          status: statusType,

        }
      })

      // Get User Info
      .addCase(getUserInfoMethod.pending, (state) => {
        state.status.isLoading = true
        state.status.isSuccess = false
        state.status.isError = false
      })
      .addCase(getUserInfoMethod.fulfilled, (state) => {
        state.status.isLoading = false
        state.status.isSuccess = true
        state.status.isError = false
      })
      .addCase(getUserInfoMethod.rejected, (state) => {
        state.status.isLoading = false
        state.status.isSuccess = false
        state.status.isError = true
      })

      // Change User Info
      .addCase(changeUserInfoMethod.pending, (state) => {
        state.status.isLoading = true
        state.status.isSuccess = false
        state.status.isError = false
      })
      .addCase(changeUserInfoMethod.fulfilled, (state) => {
        state.status.isLoading = false
        state.status.isSuccess = true
        state.status.isError = false
      })
      .addCase(changeUserInfoMethod.rejected, (state) => {
        state.status.isLoading = false
        state.status.isSuccess = false
        state.status.isError = true
      })

      // Change Password
      .addCase(changePassword.pending, (state) => {
        state.status.isLoading = true
        state.status.isSuccess = false
        state.status.isError = false
      })
      .addCase(changePassword.fulfilled, (state) => {
        state.status.isLoading = false
        state.status.isSuccess = true
        state.status.isError = false

      })
      .addCase(changePassword.rejected, (state) => {
        state.status.isLoading = false
        state.status.isSuccess = false
        state.status.isError = true

      })

      // Forgot Password
      .addCase(forgotPassword.pending, (state) => {
        state.status.isLoading = true
        state.status.isSuccess = false
        state.status.isError = false
      })
      .addCase(forgotPassword.fulfilled, (state) => {
        state.status.isLoading = false
        state.status.isSuccess = true
        state.status.isError = false
      })
      .addCase(forgotPassword.rejected, (state) => {
        state.status.isLoading = false
        state.status.isSuccess = false
        state.status.isError = true
      })

      // Verify Password
      .addCase(verifyOtp.pending, (state) => {
        state.status.isLoading = true
        state.status.isSuccess = false
        state.status.isError = false
      })
      .addCase(verifyOtp.fulfilled, (state) => {
        state.status.isLoading = false
        state.status.isSuccess = true
        state.status.isError = false
      })
      .addCase(verifyOtp.rejected, (state) => {
        state.status.isLoading = false
        state.status.isSuccess = false
        state.status.isError = true
      })

      // Change Password With OTP
      .addCase(changePasswordWithOtpMethod.pending, (state) => {
        state.status.isLoading = true
        state.status.isSuccess = false
        state.status.isError = false
      })
      .addCase(changePasswordWithOtpMethod.fulfilled, (state) => {
        state.status.isLoading = false
        state.status.isSuccess = true
        state.status.isError = false
      })
      .addCase(changePasswordWithOtpMethod.rejected, (state) => {
        state.status.isLoading = false
        state.status.isSuccess = false
        state.status.isError = true
      })

  }
})

export const { setLogin, setSignup, resetState, setLoading, setGeneralUserInfo } = userSlice.actions
export default userSlice.reducer