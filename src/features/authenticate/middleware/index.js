import { createAsyncThunk } from '@reduxjs/toolkit';
import { AuthApiMethod } from '../api';

export const loginMethod = createAsyncThunk(
  'auth/loginMethod',
  async (data, thunkAPI) => {
    try {
      const response = await AuthApiMethod.login(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const signupMethod = createAsyncThunk(
  'auth/signupMethod',
  async (data, thunkAPI) => {
    try {
      const response = await AuthApiMethod.signup(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const getGeneralUserInfoMethod = createAsyncThunk(
  'user/getGeneralUserInfoMethod',
  async (data, thunkAPI) => {
    try {
      const response = await AuthApiMethod.getGeneralUserInfo();
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const getUserInfoMethod = createAsyncThunk(
  'user/getUserInfoMethod',
  async (data, thunkAPI) => {
    try {
      const response = await AuthApiMethod.getProfile();
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const changeUserInfoMethod = createAsyncThunk(
  'user/changeUserInfoMethod',
  async (data, thunkAPI) => {
    try {
      const response = await AuthApiMethod.changeProfile(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const changePassword = createAsyncThunk(
  'user/changePassword',
  async (data, thunkAPI) => {
    try {
      const response = await AuthApiMethod.changePassword(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const forgotPassword = createAsyncThunk(
  'user/forgotPassword',
  async (data, thunkAPI) => {
    try {
      const response = await AuthApiMethod.forgetPassword(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const verifyOtp = createAsyncThunk(
  'user/verifyOtp',
  async (data, thunkAPI) => {
    try {
      const response = await AuthApiMethod.verifyOtp(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const changePasswordWithOtpMethod = createAsyncThunk(
  'user/changePasswordWithOTP',
  async (data, thunkAPI) => {
    try {
      const response = await AuthApiMethod.changePasswordWithOtp(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);
