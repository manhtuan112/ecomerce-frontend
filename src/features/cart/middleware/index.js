import { createAsyncThunk } from '@reduxjs/toolkit';
import { CartApiMethod } from '../api';

export const getListCartMethod = createAsyncThunk(
  'cart/getListCartMethod',
  async (data, thunkAPI) => {
    try {
      const response = await CartApiMethod.getListCart(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const addToCartMethod = createAsyncThunk(
  'cart/addToCartMethod',
  async (data, thunkAPI) => {
    try {
      const response = await CartApiMethod.addToCart(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const updateCartMethod = createAsyncThunk(
  'cart/updateCartMethod',
  async (data,  thunkAPI) => {
    try {
      const response = await CartApiMethod.updateCart(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }
  }
);

export const deleteCartMethod = createAsyncThunk(
  'cart/deleteCartMethod',
  async (data,  thunkAPI) => {
    try {
      const response = await CartApiMethod.deleteCart(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }
  }
);