import { createSlice } from '@reduxjs/toolkit'
import { getLocalStorage, setLocalStorage } from '../../../utils/localStorage'
import { addToCartMethod, deleteCartMethod, getListCartMethod } from '../middleware'

const statusType = {
  isLoading: false,
  isSuccess: false,
  isError: false
}

const initialState = {
  status: statusType,
  numberCart: 0,
  listCartDelete: []
}

export const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    setListCartDelete: (state, action) =>{
      state.listCartDelete = action.payload
      setLocalStorage('listCartDelete', action.payload)
    },
    setNumberCart: (state, active) =>{
      let number_cart = getLocalStorage('numberCart')
      let length_listCartDelete = getLocalStorage('listCartDelete').split(',').length
      if (number_cart !== null){
        number_cart -= length_listCartDelete
        setLocalStorage('numberCart', number_cart)
      }

      state.numberCart = number_cart
      return state
      
    }
  },
  extraReducers: (builder) => {
    builder
      // Add To Cart
      .addCase(addToCartMethod.pending, (state, action) => {
        state.status.isLoading = true
        state.status.isError = false
        state.status.isSuccess = false
      })
      .addCase(addToCartMethod.fulfilled, (state, action) => {
        state.status.isLoading = false
        state.status.isError = false
        state.status.isSuccess = true
      })
      .addCase(addToCartMethod.rejected, (state, action) => {
        state.status.isLoading = false
        state.status.isError = true
        state.status.isSuccess = false
      })

      // Get List View Cart
      .addCase(getListCartMethod.pending, (state, action) => {
        state.status.isLoading = true
        state.status.isError = false
        state.status.isSuccess = false
      })
      .addCase(getListCartMethod.fulfilled, (state, action) => {
        state.numberCart = action.payload.data.length
        setLocalStorage('numberCart', action.payload.data.length)
        state.status.isLoading = false
        state.status.isError = false
        state.status.isSuccess = true
      })
      .addCase(getListCartMethod.rejected, (state, action) => {
        state.status.isLoading = false
        state.status.isError = true
        state.status.isSuccess = false
      })

      // Get List View Cart
      .addCase(deleteCartMethod.pending, (state, action) => {
        state.status.isLoading = true
        state.status.isError = false
        state.status.isSuccess = false
      })
      .addCase(deleteCartMethod.fulfilled, (state, action) => {
        state.numberCart -= 1
        state.status.isLoading = false
        state.status.isError = false
        state.status.isSuccess = true
      })
      .addCase(deleteCartMethod.rejected, (state, action) => {
        state.status.isLoading = false
        state.status.isError = true
        state.status.isSuccess = false
      })


  }
})

export const { setListCartDelete, setNumberCart } = cartSlice.actions
export default cartSlice.reducer