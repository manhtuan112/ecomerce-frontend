import { createAxiosInstance } from '../../../utils/axiosInstance'

export const CartApiMethod = {
  baseUrl: import.meta.env.VITE_API_URL_CART || process.env.REACT_APP_API_URL,
  addToCart: (data) => {
    const axiosInstance = createAxiosInstance(CartApiMethod.baseUrl)
    return axiosInstance.post('/cart/add_to_cart/', data)
  },

  getListCart: (data) => {
    const axiosInstance = createAxiosInstance(CartApiMethod.baseUrl)
    return axiosInstance.get(`/cart/show/${data}/`)
  },

  updateCart: (data) => {
    const axiosInstance = createAxiosInstance(CartApiMethod.baseUrl)
    return axiosInstance.put(`/cart/update/${data}/`)
  },

  deleteCart: (data) => {
    const axiosInstance = createAxiosInstance(CartApiMethod.baseUrl)
    return axiosInstance.delete(`/cart/update/${data}/`)
  },
}