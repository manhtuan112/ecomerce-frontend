import { createAxiosInstance } from '../../../utils/axiosInstance'

export const OrderApiMethod = {
  baseUrl: import.meta.env.VITE_API_URL_ORDER || process.env.REACT_APP_API_URL,
  baseUrlPayment: import.meta.env.VITE_API_URL_PAYMENT || process.env.REACT_APP_API_URL,
  createOrder: (data) => {
    const axiosInstance = createAxiosInstance(OrderApiMethod.baseUrl)
    return axiosInstance.post('/order/create/', data)
  },

  updateOrder: (data) => {
    const axiosInstance = createAxiosInstance(OrderApiMethod.baseUrl)
    return axiosInstance.put('/order/update/', data)
  },

  getOrderInfo: (data) => {
    const axiosInstance = createAxiosInstance(OrderApiMethod.baseUrl)
    return axiosInstance.get(`/order/get_order_by_id/${data}/`)
  },

  paymentByVNPay: (data) => {
    const axiosInstance = createAxiosInstance(OrderApiMethod.baseUrlPayment)
    return axiosInstance.post(`/payment/create/`, data)
  },

  paymentByZaloPay: (data) => {
    const axiosInstance = createAxiosInstance(OrderApiMethod.baseUrlPayment)
    return axiosInstance.post(`/payment/create_zalopay/`, data)
  },

  deleteOrder: (data) => {
    const axiosInstance = createAxiosInstance(OrderApiMethod.baseUrl)
    return axiosInstance.delete(`/cart/update/${data}/`)
  },
}