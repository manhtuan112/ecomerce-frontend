import { createSlice } from '@reduxjs/toolkit'
import { createOrderMethod, getOrderInfoMethod, paymentVNPayMethod, paymentZaloPayMethod, updateOrderMethod } from '../middleware'

const statusType = {
  isLoading: false,
  isSuccess: false,
  isError: false
}

const initialState = {
  status: statusType,
}

export const orderSlice = createSlice({
  name: 'order',
  initialState,
  reducers: {
  },
  extraReducers: (builder) => {
    builder
      // Add To Order
      .addCase(createOrderMethod.pending, (state, action) => {
        state.status.isLoading = true
        state.status.isError = false
        state.status.isSuccess = false
      })
      .addCase(createOrderMethod.fulfilled, (state, action) => {
        state.status.isLoading = false
        state.status.isError = false
        state.status.isSuccess = true
      })
      .addCase(createOrderMethod.rejected, (state, action) => {
        state.status.isLoading = false
        state.status.isError = true
        state.status.isSuccess = false
      })

      // Add To Order
      .addCase(updateOrderMethod.pending, (state, action) => {
        state.status.isLoading = true
        state.status.isError = false
        state.status.isSuccess = false
      })
      .addCase(updateOrderMethod.fulfilled, (state, action) => {
        state.status.isLoading = false
        state.status.isError = false
        state.status.isSuccess = true
      })
      .addCase(updateOrderMethod.rejected, (state, action) => {
        state.status.isLoading = false
        state.status.isError = true
        state.status.isSuccess = false
      })

      // Get List View Order
      .addCase(getOrderInfoMethod.pending, (state, action) => {
        state.status.isLoading = true
        state.status.isError = false
        state.status.isSuccess = false
      })
      .addCase(getOrderInfoMethod.fulfilled, (state, action) => {
        state.status.isLoading = false
        state.status.isError = false
        state.status.isSuccess = true
      })
      .addCase(getOrderInfoMethod.rejected, (state, action) => {
        state.status.isLoading = false
        state.status.isError = true
        state.status.isSuccess = false
      })

      // // Get List View Order
      // .addCase(deleteOrderMethod.pending, (state, action) => {
      //   state.status.isLoading = true
      //   state.status.isError = false
      //   state.status.isSuccess = false
      // })
      // .addCase(deleteOrderMethod.fulfilled, (state, action) => {
      //   state.numberOrder -= 1
      //   state.status.isLoading = false
      //   state.status.isError = false
      //   state.status.isSuccess = true
      // })
      // .addCase(deleteOrderMethod.rejected, (state, action) => {
      //   state.status.isLoading = false
      //   state.status.isError = true
      //   state.status.isSuccess = false
      // })

      // Payment by VNPAY
      // Get List View Order
      .addCase(paymentVNPayMethod.pending, (state, action) => {
        state.status.isLoading = true
        state.status.isError = false
        state.status.isSuccess = false
      })
      .addCase(paymentVNPayMethod.fulfilled, (state, action) => {
        state.status.isLoading = false
        state.status.isError = false
        state.status.isSuccess = true
      })
      .addCase(paymentVNPayMethod.rejected, (state, action) => {
        state.status.isLoading = false
        state.status.isError = true
        state.status.isSuccess = false
      })

      // Payment by ZaloPay
      // Get List View Order
      .addCase(paymentZaloPayMethod.pending, (state, action) => {
        state.status.isLoading = true
        state.status.isError = false
        state.status.isSuccess = false
      })
      .addCase(paymentZaloPayMethod.fulfilled, (state, action) => {
        state.status.isLoading = false
        state.status.isError = false
        state.status.isSuccess = true
      })
      .addCase(paymentZaloPayMethod.rejected, (state, action) => {
        state.status.isLoading = false
        state.status.isError = true
        state.status.isSuccess = false
      })

  }
})

export const { } = orderSlice.actions
export default orderSlice.reducer