import { createAsyncThunk } from '@reduxjs/toolkit';
import { OrderApiMethod } from '../api';

export const createOrderMethod = createAsyncThunk(
  'order/createOrderMethod',
  async (data, thunkAPI) => {
    try {
      const response = await OrderApiMethod.createOrder(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const getOrderInfoMethod = createAsyncThunk(
  'order/getOrderInfoMethod',
  async (data, thunkAPI) => {
    try {
      const response = await OrderApiMethod.getOrderInfo(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);



export const updateOrderMethod = createAsyncThunk(
  'order/updateOrderMethod',
  async (data, thunkAPI) => {
    try {
      const response = await OrderApiMethod.updateOrder(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }
  }
);

export const deleteOrderMethod = createAsyncThunk(
  'order/deleteOrderMethod',
  async (data, thunkAPI) => {
    try {
      const response = await OrderApiMethod.deleteOrder(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }
  }
);


// Payment
export const paymentVNPayMethod = createAsyncThunk(
  'payment/paymentVNPayMethod',
  async (data, thunkAPI) => {
    try {
      const response = await OrderApiMethod.paymentByVNPay(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

// Payment
export const paymentZaloPayMethod = createAsyncThunk(
  'payment/paymentZaloPayMethod',
  async (data, thunkAPI) => {
    try {
      const response = await OrderApiMethod.paymentByZaloPay(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);