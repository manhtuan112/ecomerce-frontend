import { createSlice } from '@reduxjs/toolkit'
import { setLocalStorage } from '../../../utils/localStorage'
import { createReviewMethod, getListRecomendProductMethod, getListReiviewByProductMethod } from '../middleware'

const statusType = {
  isLoading: false,
  isSuccess: false,
  isError: false
}

const reviewListType = {
  avarage_rating: 0,
  Reviews: []
}

const initialState = {
  status: statusType,
  list_review: reviewListType,
  recommendProductList: []
}

export const reviewSlice = createSlice({
  name: 'review',
  initialState,
  reducers: {
  },
  extraReducers: (builder) => {
    builder
      // Get List review
      .addCase(getListReiviewByProductMethod.pending, (state, action) => {
        state.status.isLoading = true
        state.status.isError = false
        state.status.isSuccess = false
      })
      .addCase(getListReiviewByProductMethod.fulfilled, (state, action) => {
        state.status.isLoading = false
        state.status.isError = false
        state.status.isSuccess = true
        if (Array.isArray(action.payload.data)) { // response data when not found commend is arrray []
          state.list_review = {
            avarage_rating: 0,
            Reviews: []
          }
        } else {
          state.list_review = action.payload.data
        }
      })
      .addCase(getListReiviewByProductMethod.rejected, (state, action) => {
        state.status.isLoading = false
        state.status.isError = true
        state.status.isSuccess = false
      })



      // Create review 
      .addCase(createReviewMethod.pending, (state, action) => {
        state.status.isLoading = true
        state.status.isError = false
        state.status.isSuccess = false
      })
      .addCase(createReviewMethod.fulfilled, (state, action) => {
        state.status.isLoading = false
        state.status.isError = false
        state.status.isSuccess = true

      })
      .addCase(createReviewMethod.rejected, (state, action) => {
        state.status.isLoading = false
        state.status.isError = true
        state.status.isSuccess = false
      })

      // Get List Product Recommend
      .addCase(getListRecomendProductMethod.pending, (state, action) => {
        state.status.isLoading = true
        state.status.isError = false
        state.status.isSuccess = false
      })
      .addCase(getListRecomendProductMethod.fulfilled, (state, action) => {
        state.status.isLoading = false
        state.status.isError = false
        state.status.isSuccess = true
        state.recommendProductList = action.payload.data
      })
      .addCase(getListRecomendProductMethod.rejected, (state, action) => {
        state.status.isLoading = false
        state.status.isError = true
        state.status.isSuccess = false
      })



  }
})

export const { } = reviewSlice.actions
export default reviewSlice.reducer