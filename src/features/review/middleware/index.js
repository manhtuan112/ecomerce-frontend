import { createAsyncThunk } from '@reduxjs/toolkit';
import { ReviewApiMethod } from '../api';

export const getListReiviewByProductMethod = createAsyncThunk(
  'review/getListReviewByProductMethod',
  async (data, thunkAPI) => {
    try {
      const response = await ReviewApiMethod.getListReivewByProduct(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }
  }
);


export const createReviewMethod = createAsyncThunk(
  'review/createReview',
  async (data, thunkAPI) => {
    try {
      const response = await ReviewApiMethod.createReview(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }
  }
);



export const getListRecomendProductMethod = createAsyncThunk(
  'review/getListRecomendProductMethod',
  async (data, thunkAPI) => {
    try {
      const response = await ReviewApiMethod.getListRecommendProduct(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }
  }
);