import { createAxiosInstance } from '../../../utils/axiosInstance'

export const ReviewApiMethod = {
  baseUrl: import.meta.env.VITE_API_URL_REVIEW || process.env.REACT_APP_API_URL,
  getListReivewByProduct: (data) => {
    const axiosInstance = createAxiosInstance(ReviewApiMethod.baseUrl)
    return axiosInstance.get(`/review/get_list_review_by_product/${data}/`)
  },

  createReview: (data) => {
    const axiosInstance = createAxiosInstance(ReviewApiMethod.baseUrl)
    return axiosInstance.post('/review/create/', data)
  },

  getListRecommendProduct: (data) => {
    const axiosInstance = createAxiosInstance(ReviewApiMethod.baseUrl)
    return axiosInstance.get(`/review/collaborative/${data}/`)
  },
  

}