import { createAxiosInstance } from '../../../utils/axiosInstance'

export const chatbotAPI = {
  baseUrl: import.meta.env.VITE_API_URL_CHAT || process.env.REACT_APP_API_URL,
  chatbot: (data) => {
    console.log(data);
    const axiosInstance = createAxiosInstance(chatbotAPI.baseUrl)
    return axiosInstance.post('', data)
  },
}