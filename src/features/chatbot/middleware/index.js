import { createAsyncThunk } from '@reduxjs/toolkit';
import { chatbotAPI } from '../api';

export const chatbotMethod = createAsyncThunk(
  'chatbot',
  async (data, thunkAPI) => {
    try {
      const response = await chatbotAPI.chatbot(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);
