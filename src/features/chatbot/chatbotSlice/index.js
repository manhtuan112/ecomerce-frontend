import { createSlice } from '@reduxjs/toolkit'
import { chatbotMethod } from '../middleware'
import { getLocalStorage, removeLocalStorage, setLocalStorage } from '../../../utils/localStorage'


const getChatbotListFromLocalStorage = () => {
  if (getLocalStorage('listChatbot')) {
    return JSON.parse(getLocalStorage('listChatbot'))
  } else {
    setLocalStorage('listChatbot', JSON.stringify([{
      id: 1,
      sender: 'Chatbot',
      message: 'Hello my friend, I am Chatbot from <b>T Store</b>. How can I help you?',
      direction: 'incoming',
      position: 'last'
    }])
    )
    return [{
      id: 1,
      sender: 'Chatbot',
      message: 'Hello my friend, I am Chatbot from <b>T Store</b>. How can I help you?',
      direction: 'incoming',
      position: 'last'
    }]
  }
}


const initialState = {
  listChatbot: getChatbotListFromLocalStorage(),
}

export const chatbotSlice = createSlice({
  name: 'chatbot',
  initialState,
  reducers: {
    addMessageToList: (state, action) => {
      state.listChatbot.push(action.payload)
      setLocalStorage('listChatbot', JSON.stringify(state.listChatbot))
    },
    deleteListChatbot: (state, action) => {
      removeLocalStorage('listChatbot')
      return {
        listChatbot: [{
          id: 1,
          sender: 'Chatbot',
          message: 'Hello my friend, I am Chatbot from <b>T Store</b>. How can I help you?',
          direction: 'incoming',
          position: 'last'
        }]
      }
    }},
    extraReducers: (builder) => {
      builder
        // Chatbot API
        .addCase(chatbotMethod.pending, (state, action) => {
          
        })
        .addCase(chatbotMethod.fulfilled, (state, action) => {
          
        })
        .addCase(chatbotMethod.rejected, (state, action) => {
          
        })



    }
  })

export const { addMessageToList, deleteListChatbot } = chatbotSlice.actions
export default chatbotSlice.reducer