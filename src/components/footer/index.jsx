import React from 'react'
import { FaRegPaperPlane, FaLinkedin } from 'react-icons/fa'
import { FaGithub, FaYoutube, FaInstagram } from 'react-icons/fa6'
import './style.scss'
import { Link } from 'react-router-dom'
function Footer() {
  return (
    <>
      <footer className='py-4'>
        <div className='container-xxl'>
          <div className='row align-items-center'>
            <div className='col-5'>
              <div className='footer-top-data d-flex gap-15 align-items-center text-white'>
                <FaRegPaperPlane className='fs-4' />
                <h2 className='mb-0'>Sign Up for Newsletter</h2>
              </div>
            </div>
            <div className='col-7'>
              <div className='input-group'>
                <input
                  type='text'
                  className='form-control py-1'
                  placeholder='Nhap thong tin Email'
                  aria-label='Nhap thong tin Email'
                />
                <span className='input-group-text p-2' id='basic-addon2'>
                  Subscribe
                </span>
              </div>
            </div>
          </div>
        </div>
      </footer>
      <footer className='py-4'>
        <div className='container-xxl'>
          <div className='row'>
            <div className='col-4'>
              <h4 className='text-white mb-4'>Contact Us</h4>
              <div className='footer-link'>
                <address className='text-white fs-6'>
                  Hnoi: 277 Near Voll chopal,
                  <br /> Sonipat, Haryana <br />
                  PinCode: 423423
                </address>
                <div>
                  <Link to='tel: +84 0388864256' className='mt-3 d-block mb-1 text-white'>
                    +84 388864256
                  </Link>
                </div>
                <div>
                  <Link to='mailto: manhtuan1122001@gmail.com' className='mt-2 d-block mb-0 text-white'>
                    manhtuan1122001@gmail.com
                  </Link>
                </div>
                <div className='social_icons d-flex align-items-center gap-30 mt-4'>
                  <Link className='text-white' to='/'>
                    <FaLinkedin className='fs-4' />
                  </Link>
                  <Link className='text-white' to='/'>
                    <FaGithub className='fs-4' />
                  </Link>
                  <Link className='text-white' to='/'>
                    <FaYoutube className='fs-4' />
                  </Link>
                  <Link className='text-white' to='/'>
                    <FaInstagram className='fs-4' />
                  </Link>
                </div>
              </div>
            </div>
            <div className='col-3'>
              <h4 className='text-white mb-4'>Information</h4>
              <div className='footer-link d-flex flex-column'>
                <Link to='/privacy-policy' className='text-white py-2 mb-1'>
                  Privacy Policy
                </Link>
                <Link to='/refund-policy' className='text-white py-2 mb-1'>
                  Refund Policy
                </Link>
                <Link to='/shipping-policy' className='text-white py-2 mb-1'>
                  Shipping Policy
                </Link>
                <Link to='/term-conditions' className='text-white py-2 mb-1'>
                  Term & Conditions
                </Link>
                <Link to='/blogs' className='text-white py-2 mb-1'>
                  Blogs
                </Link>
              </div>
            </div>
            <div className='col-3'>
              <h4 className='text-white mb-4'>Account</h4>
              <div className='footer-link d-flex flex-column'>
                <Link className='text-white py-2 mb-1'>Laptops</Link>
                <Link className='text-white py-2 mb-1'>Laptops</Link>
                <Link className='text-white py-2 mb-1'>Laptops</Link>
              </div>
            </div>
            <div className='col-2'>
              <h4 className='text-white mb-4'>Quick Links</h4>
              <div className='footer-link d-flex flex-column'>
                <Link className='text-white py-2 mb-1'>Laptops</Link>
                <Link className='text-white py-2 mb-1'>Laptops</Link>
                <Link className='text-white py-2 mb-1'>Laptops</Link>
                <Link className='text-white py-2 mb-1'>Laptops</Link>
              </div>
            </div>
          </div>
        </div>
      </footer>
      <footer className='py-4'>
        <div className='container-xxl'>
          <div className='row'>
            <div className='col-12'>
              <div className='text-center text-white mb-0'>
                &copy; {new Date().getFullYear()} Lorem ipsum dolor sit amet consectetur.
              </div>
            </div>
          </div>
        </div>
      </footer>
    </>
  )
}

export default Footer
