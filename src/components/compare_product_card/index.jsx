import React from 'react'
import './style.scss'
import { ImCancelCircle } from 'react-icons/im'
import Color from '../../components/color'
function CompareProductCard() {
  return (
    <div className='compare-product-card position-relative'>
      <ImCancelCircle className='position-absolute cross fs-5' />
      <div className='product-card-image d-flex justify-content-center'>
        <img src='https://www.lg.com/vn/images/tivi/md07551233/gallery/D-01.jpg' alt='image' />
      </div>
      <div className='compare-product-details'>
        <h5 className='title'>Lorem ipsum dolor sit amet consectetur adipisicing.</h5>
        <h6 className='price my-3'>12.000.000VND</h6>

        <div>
          <div className='product-detail'>
            <h5>Brand: </h5>
            <p>Sony</p>
          </div>
          <div className='product-detail'>
            <h5>Type: </h5>
            <p>Infinity</p>
          </div>
          <div className='product-detail'>
            <h5>Availablity: </h5>
            <p>In Stock</p>
          </div>
          <div className='product-detail'>
            <h5>Color: </h5>
            <Color />
          </div>
          <div className='product-detail'>
            <h5>Size: </h5>
            <div className='d-flex gap-10'>
              <p>S</p>
              <p>M</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CompareProductCard
