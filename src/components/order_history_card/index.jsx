import React from 'react'
import CartItem from '../cart_item'
import OrderItem from '../order_item'

function OrderHistoryCard() {
  return (
    <section className='order-history-section box-shadown'>
      <div className='col-12'>
        <div className='cart-header p-3 d-flex justify-content-between align-content-center'>
          <h4 className='w-25 ms-5'>Product</h4>
          <h4 className='cart-col-2'>Order ID</h4>
          <h4 className='cart-col-3'>Quantity</h4>
          <h4 className='cart-col-4'>Total</h4>
          <h4 className='cart-col-5'>Status</h4>
        </div>
        <OrderItem />
      </div>
    </section>
  )
}

export default OrderHistoryCard
