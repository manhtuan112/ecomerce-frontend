import React from 'react'
import './style.scss'
function CategoryItem({ data }) {
  return (
    <div className='d-flex align-items-center justify-content-center gap-10'>
      <div>
        <h6 className='mb-0'>{data.name}</h6>
      </div>
      <div className='category-img d-flex align-items-center justify-content-center'>
        <img src={data.thumbnail} alt='camera' />
      </div>
    </div>
  )
}

export default CategoryItem
