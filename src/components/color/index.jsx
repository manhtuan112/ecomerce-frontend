import React from 'react'
import './style.scss'
function Color(props) {
  const { data } = props
  const { isColorActive, handleChooseColor } = props
  if (isColorActive && handleChooseColor) {
    return (
      <>
        <ul className='colors ps-0'>
          {data.map((item, index) => (
            <li
              key={index}
              style={{ backgroundColor: item }}
              onClick={() => handleChooseColor(item)}
              className={`${isColorActive === item ? 'color-active' : ''}`}
            ></li>
          ))}
        </ul>
      </>
    )
  } else {
    return (
      <>
        <ul className='colors ps-0'>
          {data.map((item, index) => (
            <li key={index} style={{ backgroundColor: item }}></li>
          ))}
        </ul>
      </>
    )
  }
}

export default Color
