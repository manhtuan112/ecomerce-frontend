import React, { useEffect, useState } from 'react'
import './style.scss'
import UpdateProfileModal from '../update_profile_modal'
import { getUserInfoMethod } from '../../features/authenticate/middleware'
import { useDispatch, useSelector } from 'react-redux'
import { unwrapResult } from '@reduxjs/toolkit'
import Swal from 'sweetalert2'
import { convertDate } from '../../features/common'

function PersonalDetails() {
  const dispatch = useDispatch()
  const [personalInfo, setPersonalInfo] = useState(null)
  const [showModal, setShowModal] = useState(false)
  useEffect(() => {
    const getDataPersonalDetails = async () => {
      try {
        const resultAction = await dispatch(getUserInfoMethod())
        const user = unwrapResult(resultAction)
        setPersonalInfo(user.data)
      } catch (e) {
        Swal.fire({
          icon: 'error',
          title: 'Get Personal Information Failed!!!',
          text: e
        })
      }
    }
    getDataPersonalDetails()
  }, [])


  return (
    <>
      {personalInfo && (
        <section className='user-info-section box-shadown'>
          <div className='avatar-and-username-wrapper d-flex align-items-center gap-30'>
            <div className='user-avatar w-25 rounded-5'>
              <img
                src={
                  personalInfo.avatar
                    ? personalInfo.avatar
                    : 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png'
                }
                alt='image'
              />
            </div>
            <div className='username-welcome '>
              <div>
                <span>Welcome, </span> {personalInfo.username}
              </div>
              <div className='roll'>Customer</div>
            </div>
          </div>
          <div className='user-info-list'>
            <div className='row'>
              <div className='user-info-line col-6'>
                <span>First Name:</span>
                <span>{personalInfo.first_name}</span>
              </div>
              <div className='user-info-line col-6'>
                <span>Last Name:</span>
                <span>{personalInfo.last_name}</span>
              </div>
              <div className='user-info-line col-6'>
                <span>Username:</span>
                <span>{personalInfo.username}</span>
              </div>
              <div className='user-info-line col-6'>
                <span>Date of Birth:</span>
                <span>{personalInfo.date_of_birth ? convertDate(personalInfo.date_of_birth, true) : 'No information'}</span>
              </div>
              <div className='user-info-line col-6'>
                <span>Telephone:</span>
                <span>{personalInfo.telephoneNumber ? personalInfo.telephoneNumber : 'No information'}</span>
              </div>
              <div className='user-info-line col-6'>
                <span>Date Join:</span>
                <span>{personalInfo.date_joined ? convertDate(personalInfo.date_joined) : 'No information'}</span>
              </div>
              <div className='user-info-line col-12 one-line'>
                <span>Email:</span>
                <span>{personalInfo.email}</span>
              </div>
              <div className='user-info-line col-12 one-line'>
                <span>Address:</span>
                <span>
                  {personalInfo.address
                    ? `${personalInfo.address.home_number}, ${personalInfo.address.street}, ${personalInfo.address.state}, ${personalInfo.address.city}`
                    : 'No information'}
                </span>
              </div>
            </div>
          </div>
          <div className='d-flex align-items-center justify-content-center'>
            <button className='button' onClick={() => setShowModal(true)}>
              Change User Information
            </button>
          </div>
        </section>
      )}
      {showModal && (
        <UpdateProfileModal setShowModal={setShowModal} personalInfo={personalInfo} setPersonalInfo={setPersonalInfo} />
      )}
    </>
  )
}

export default PersonalDetails
