import React, { useCallback, useEffect, useState } from 'react'
import ReactStars from 'react-rating-stars-component'
import './style.scss'
import { Link } from 'react-router-dom'
import { convertMoneyFormat } from '../../features/common'

export const CountdownTimer = ({ days, hours, minutes, seconds }) => {
  return (
    <div className='discount-till d-flex align-items-center gap-10'>
      <p className='mb-0'>
        <b>{days}</b> days
      </p>
      <div className='d-flex gap-10 align-items-center'>
        <span className='badge rounded-circle bg-danger timer'>{hours}</span>:
        <span className='badge rounded-circle bg-danger timer'>{minutes}</span>:
        <span className='badge rounded-circle bg-danger timer'>{seconds}</span>
      </div>
    </div>
  )
}

function SpecialProductCard({ data, list_category }) {
  const [category_name, setCategory_name] = useState()
  const [duration, setDuration] = useState()

  const getFormattedTime = (millisecond) => {
    let total_seconds = parseInt(Math.floor(millisecond / 1000))
    let total_minutes = parseInt(Math.floor(total_seconds / 60))
    let total_hours = parseInt(Math.floor(total_minutes / 60))
    let days = parseInt(Math.floor(total_hours / 24))

    let seconds = parseInt(total_seconds % 60)
    let minutes = parseInt(total_minutes % 60)
    let hours = parseInt(total_hours % 24)
    return <CountdownTimer days={days} hours={hours} minutes={minutes} seconds={seconds} />
  }
  useEffect(() => {
    const found = list_category.find((element) => element.id === data.category)
    setCategory_name(found?.name)
    const currentMilliseconds = Date.now()

    const dateTimeString = data.discount.effective_time
    const dateTime = new Date(dateTimeString)

    const milliseconds = dateTime.getTime()
    setDuration(milliseconds - currentMilliseconds)
  }, [])

  useEffect(() => {
    let timer = setTimeout(() => {
      if (duration > 0) {
        setDuration(duration - 1000)
      } else {
        return null
      }
    }, 1000)

    return () => {
      if (timer) {
        clearTimeout(timer)
      }
    }
  }, [duration])

  return (
    <div className='col-6 mb-3'>
      <div className='special-product-card'>
        <div className='d-flex justify-content-between gap-30'>
          <div className='product-image'>
            <img
              src={
                data.image
                  ? data.image
                  : 'https://media.licdn.com/dms/image/D4E12AQFAx030aGgdSQ/article-cover_image-shrink_720_1280/0/1693505218173?e=2147483647&v=beta&t=YC1XVE2ljS94xV45ijOh3BuguQJtw7d6Eoxet4JQuy0'
              }
              alt='anh'
              className='img-fluid'
            />
          </div>
          <div className='special-product-content w-50'>
            <h5 className='brand'>{category_name ? category_name : 'No category'}</h5>
            <h6 className='title'>{data.title}</h6>
            <ReactStars count={5} size={24} value={parseFloat(data.startRating)} edit={false} activeColor='#ffd700' />
            <p className='price'>
              <span className='red-p'>
                {convertMoneyFormat(Math.round((data.price * (100 - data.discount.discount_percent)) / 100))} VND
              </span>{' '}
              <span className='discount-price-card'>
                <strike>{convertMoneyFormat(data.price)}</strike>
                <span>-{parseInt(data.discount.discount_percent)}%</span>
              </span>
            </p>
            {!(duration <= 0) && <div>{getFormattedTime(duration)}</div>}
            <div className='prod-count my-3'>
              <p>Quantity Sold: {data.quantity_sold}</p>
            </div>
            <Link className='button' to={`product/${data.id}`}>View More</Link>
          </div>
        </div>
      </div>
    </div>
  )
}

export default SpecialProductCard
