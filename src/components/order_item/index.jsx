import React from 'react'
import Color from '../color'
import { convertMoneyFormat } from '../../features/common'

function OrderItem() {
  return (
    <div className='cart-data mb-2 p-3 d-flex justify-content-between align-items-center'>
      <div className='cart-col-1 d-flex gap-15 align-items-center'>
        <div className='w-25'>
          <img
            src={`https://cdn2.cellphones.com.vn/insecure/rs:fill:0:358/q:80/plain/https://cellphones.com.vn/media/catalog/product/i/p/iphone-15-plus-256gb-color-black-image.png`}
            alt='image'
            className='img-fluid'
          />
        </div>
        <div className='w-75'>
          <h5 className='title'>iPhone 15 Plus 128GB | Chính hãng VN/A</h5>
          <div className='d-flex align-items-center gap-15'>
            <p>Color: </p>
            <Color data={['#fff']} />
          </div>
          <p className='size'>Size: 256 GB</p>
        </div>
      </div>
      <div className='cart-col-4'>
        <h5 className='price'>#40</h5>
      </div>
      <div className='cart-col-4 d-flex align-items-center gap-15'>
        <div className='w-50'>
          <input className='form-control' type='number' name='' id='' defaultValue={1} min={1} max={10} readOnly />
        </div>
      </div>
      <div className='cart-col-4'>
        <h5 className='price'>{convertMoneyFormat(25090000)} VND</h5>
      </div>
      <div className='cart-col-4 d-flex gap-10 justify-content-end'>
        <span class='badge bg-success'>Payment Success</span>
      </div>
    </div>
  )
}

export default OrderItem
