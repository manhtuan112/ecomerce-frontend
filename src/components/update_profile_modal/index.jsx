import React, { useEffect, useState } from 'react'
import './style.scss'
import SectionWrappep from '../section_wrapper'
import { useDispatch, useSelector } from 'react-redux'
import { changeUserInfoMethod } from '../../features/authenticate/middleware'
import { unwrapResult } from '@reduxjs/toolkit'
import Swal from 'sweetalert2'
import { ref, uploadBytes, listAll, getDownloadURL } from 'firebase/storage'
import { storage } from '../../../firebaseConfig'
import { v4 } from 'uuid'
import { getCurrentDateTimeFormatted } from '../../features/common'
import { setGeneralUserInfo, setLoading } from '../../features/authenticate/userSlice'
import Loading from '../../pages/loading'

function UpdateProfileModal({ setShowModal, personalInfo, setPersonalInfo }) {
  const { isLoading } = useSelector((state) => state.user.status)
  const currentPersonalInfo = personalInfo
  const [info, setInfo] = useState(personalInfo)
  const [selectedImage, setSelectedImage] = useState(personalInfo.avatar)
  const [isAvatar, setIsAvatar] = useState(true)
  const uploadImage = () => {
    return new Promise((resolve, reject) => {
      if (selectedImage === null) return reject('No image selected')
      const imageRef = ref(storage, `avatar/${v4() + '_' + selectedImage.name + '_' + getCurrentDateTimeFormatted()}`)
      dispatch(setLoading())

      uploadBytes(imageRef, selectedImage)
        .then((res) => {
          getDownloadURL(res.ref)
            .then((url) => {
              resolve(url)
            })
            .catch((e) => {
              reject(e)
            })
        })
        .catch((e) => {
          reject(e)
        })
    })
  }

  const dispatch = useDispatch()

  const handleUpdateProfile = async () => {
    const data = {}
    for (let key of Object.keys(currentPersonalInfo)) {
      if (currentPersonalInfo[key] !== info[key]) {
        data[key] = info[key]
      }
    }
    let url_image = null
    if (!isAvatar) {
      try {
        url_image = await uploadImage()
        data['avatar'] = url_image
      } catch (e) {
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: e
        })
      }
    }

    try {
      const resultAction = await dispatch(changeUserInfoMethod(data))
      const result = unwrapResult(resultAction)
      Swal.fire({
        icon: 'success',
        title: `Update Profile Successful!!!`,
        confirmButtonText: 'Oke'
      }).then((result) => {
        if (result.isConfirmed) {
          setShowModal(false)
          setPersonalInfo({ ...info, avatar: url_image ? url_image : info.avatar })
          dispatch(setGeneralUserInfo({ avatar: url_image ? url_image : info.avatar, username: info.username }))
        }
      })
    } catch (e) {
      Swal.fire({
        icon: 'error',
        title: 'Change Profile Failed!!!',
        text: e
      })
    }
  }

  return (
    <>
      {isLoading && <Loading />}
      <div className='modal-wrapper'>
        <SectionWrappep noPadding={true}>
          <div className='col-12'>
            <div className='modal-header'>
              <h5 className='modal-title' id='exampleModalLabel'>
                Update Profile
              </h5>
            </div>
          </div>
          <div className='col-xl-4'>
            <div className='card mb-4 mb-xl-0'>
              <div className='card-header'>Profile Picture</div>
              <div className='card-body text-center'>
                <img
                  className='img-account-profile rounded-circle mb-2 img-fluid'
                  src={
                    selectedImage
                      ? isAvatar
                        ? selectedImage
                        : URL.createObjectURL(selectedImage)
                      : 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png'
                  }
                  alt='image'
                />

                <div className='small font-italic text-muted mb-4'>JPG or PNG no larger than 5 MB</div>

                <button className='btn btn-primary' type='button'>
                  <label htmlFor='upload image'>Upload new image</label>
                </button>
                <input
                  type='file'
                  name='myImage'
                  id='upload image'
                  className='d-none'
                  onChange={(event) => {
                    setSelectedImage(event.target.files[0])
                    setIsAvatar(false)
                  }}
                />
              </div>
            </div>
          </div>
          <div className='col-xl-8'>
            <div className='card mb-4'>
              <div className='card-header'>Account Details</div>
              <div className='card-body'>
                <form>
                  <div className='mb-3'>
                    <label className='small mb-1' htmlFor='inputUsername'>
                      Username (how your name will appear to other users on the site)
                    </label>
                    <input
                      className='form-control'
                      id='inputUsername'
                      type='text'
                      placeholder='Enter your username'
                      value={info.username}
                      onChange={(e) => setInfo({ ...info, username: e.target.value })}
                    />
                  </div>

                  <div className='row gx-3 mb-3'>
                    <div className='col-md-6'>
                      <label className='small mb-1' htmlFor='inputFirstName'>
                        First name
                      </label>
                      <input
                        className='form-control'
                        id='inputFirstName'
                        type='text'
                        placeholder='Enter your first name'
                        value={info.first_name}
                        onChange={(e) => setInfo({ ...info, first_name: e.target.value })}
                      />
                    </div>

                    <div className='col-md-6'>
                      <label className='small mb-1' htmlFor='inputLastName'>
                        Last name
                      </label>
                      <input
                        className='form-control'
                        id='inputLastName'
                        type='text'
                        placeholder='Enter your last name'
                        value={info.last_name}
                        onChange={(e) => setInfo({ ...info, last_name: e.target.value })}
                      />
                    </div>
                  </div>
                  <div className='row gx-3 mb-3'>
                    <div className='col-md-6'>
                      <label className='small mb-1' htmlFor='inputPhone'>
                        Phone number
                      </label>
                      <input
                        className='form-control'
                        id='inputPhone'
                        type='tel'
                        placeholder='Enter your phone number'
                        value={info.telephoneNumber}
                        onChange={(e) => setInfo({ ...info, telephoneNumber: e.target.value })}
                      />
                    </div>
                    <div className='col-md-6'>
                      <label className='small mb-1' htmlFor='inputBirthday'>
                        Birthday
                      </label>
                      <input
                        className='form-control'
                        id='inputBirthday'
                        type='date'
                        name='birthday'
                        placeholder='Enter your birthday'
                        value={info.date_of_birth}
                        onChange={(e) => setInfo({ ...info, date_of_birth: e.target.value })}
                      />
                    </div>
                  </div>
                  <div className='mb-3'>
                    <label className='small mb-1' htmlFor='inputEmailAddress'>
                      Email address
                    </label>
                    <input
                      className='form-control'
                      id='inputEmailAddress'
                      type='email'
                      placeholder='Enter your email address'
                      value={info.email}
                      onChange={(e) => setInfo({ ...info, email: e.target.value })}
                    />
                  </div>

                  <label className='mb-2'>Adderess:</label>
                  <div className='row gx-3 mb-3'>
                    <div className='col-md-4'>
                      <label className='small mb-1' htmlFor='city'>
                        City
                      </label>
                      <input
                        className='form-control'
                        id='city'
                        type='text'
                        placeholder='Enter your city'
                        value={info.address ? info.address.city : ''}
                        onChange={(e) => setInfo({ ...info, address: { ...info.address, city: e.target.value } })}
                      />
                    </div>
                    <div className='col-md-4'>
                      <label className='small mb-1' htmlFor='state'>
                        State
                      </label>
                      <input
                        className='form-control'
                        id='state'
                        type='text'
                        placeholder='Enter your state'
                        value={info.address ? info.address.state : ''}
                        onChange={(e) => setInfo({ ...info, address: { ...info.address, state: e.target.value } })}
                      />
                    </div>

                    <div className='col-md-4'>
                      <label className='small mb-1' htmlFor='street'>
                        Street
                      </label>
                      <input
                        className='form-control'
                        id='street'
                        type='text'
                        placeholder='Enter your location'
                        value={info.address ? info.address.street : ''}
                        onChange={(e) => setInfo({ ...info, address: { ...info.address, street: e.target.value } })}
                      />
                    </div>
                    <div className='col-md-12'>
                      <label className='small mb-1' htmlFor='home-number'>
                        Home Number
                      </label>
                      <input
                        className='form-control'
                        id='home-number'
                        type='text'
                        placeholder='Enter your home number'
                        value={info.address ? info.address.home_number : ''}
                        onChange={(e) =>
                          setInfo({ ...info, address: { ...info.address, home_number: e.target.value } })
                        }
                      />
                    </div>
                  </div>
                </form>
              </div>
              <div className='modal-footer'>
                <button type='button' className='btn btn-secondary' onClick={() => setShowModal(false)}>
                  Close
                </button>
                <button type='button' className='btn btn-primary' onClick={handleUpdateProfile}>
                  Save changes
                </button>
              </div>
            </div>
          </div>
        </SectionWrappep>
      </div>
    </>
  )
}

export default UpdateProfileModal
