import React from 'react'
import Color from '../color'
import { convertMoneyFormat } from '../../features/common'
import './style.scss'

function CheckoutItem({ data }) {
  return (
    <div className='d-flex gap-10 align-items-center py-2'>
      <div className='w-25'>
        <div className='p-2 position-relative'>
          <span
            className='quantity_checkout'
          >
            {data.quantity}
          </span>
          <img
            src={data.image}
            alt='image'
            className='img-fluid'
          />
        </div>
      </div>
      <div className='w-50'>
        <h5 className='title mb-0'>{data.title}</h5>
        <div className='mb-0 total-price'>Size: {data.size}</div>
        <p className='mb-0 d-flex align-items-center gap-10 total-price'>Color: <Color data={[data.color]} /></p>
      </div>
      <div className='w-25'>
        <h5 style={{fontSize: '15px'}}>{convertMoneyFormat(data.price * data.quantity)} VND</h5>
      </div>
    </div>
  )
}

export default CheckoutItem
