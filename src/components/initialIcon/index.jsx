import React from 'react'
import './style.scss'
function InitialIcon({ letter, src }) {
  return (
    <div className='letter-icon'>
      <div className='circle'>
        {src ? (
          <img
            src={src}
            alt='image'
          />
        ) : (
          <span className='letter'>{letter}</span>
        )}
      </div>
    </div>
  )
}

export default InitialIcon
