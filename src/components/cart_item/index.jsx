import React from 'react'
import { MdDelete } from 'react-icons/md'
import { GrView } from 'react-icons/gr'
import './style.scss'
import Color from '../color'
import { convertMoneyFormat } from '../../features/common'
import { Link } from 'react-router-dom'
function CartItem({ data, handleCheckedCart, handleDeleteCartItem }) {
  return (
    <div className='cart-data mb-2 py-3 d-flex justify-content-between align-items-center'>
      <div className='cart-col-0 d-flex gap-15 align-items-center justify-content-center'>
        <input
          className='form-check-input'
          type='checkbox'
          checked={data.statusCheck}
          onChange={() => handleCheckedCart(data.id)}
          id={`form-check-input`}
        />
      </div>
      <div className='cart-col-1 d-flex gap-15 align-items-center'>
        <div className='w-25'>
          <img
            src={
              data.image.length === 0
                ? `https://www.freeiconspng.com/thumbs/no-image-icon/no-image-icon-4.png`
                : data.image[0].url
            }
            alt='image'
            className='img-fluid'
          />
        </div>
        <div className='w-75'>
          <h5 className='title'>{data.title}</h5>
          <div className='d-flex align-items-center gap-15'>
            <p>Color: </p>
            <Color data={[data.color]} />
          </div>
          <p className='size'>Size: {data.size}</p>
        </div>
      </div>
      <div className='cart-col-2'>
        <h5 className='price'>{convertMoneyFormat(data.price)} VND</h5>
      </div>
      <div className='cart-col-3 d-flex align-items-center gap-15'>
        <div className='w-50'>
          <input
            className='form-control'
            type='number'
            name=''
            id=''
            defaultValue={1}
            min={1}
            max={10}
            value={data.quantity}
            readOnly
          />
        </div>
      </div>
      <div className='cart-col-4'>
        <h5 className='price'>{convertMoneyFormat(data.price * data.quantity)} VND</h5>
      </div>
      <div className='cart-col-5 d-flex gap-10'>
        <div onClick={() => handleDeleteCartItem(data.id)}>
          <MdDelete className='text-danger fs-5' />
        </div>
        <div>
          <Link to={`/product/${data.product_id}`}>
            <GrView className='text-info fs-6' />
          </Link>
        </div>
      </div>
    </div>
  )
}

export default CartItem
