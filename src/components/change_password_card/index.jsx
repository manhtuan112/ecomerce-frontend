import React from 'react'
import { useFormik } from 'formik'
import * as yup from 'yup'
import Swal from 'sweetalert2'
import { unwrapResult } from '@reduxjs/toolkit'

import { useDispatch, useSelector } from 'react-redux'

import './style.scss'
import { changePassword } from '../../features/authenticate/middleware'
function ChangePasswordCard() {
  const dispatch = useDispatch()
  const status = useSelector((state) => state.user.status)
  let schema = yup.object().shape({
    currentPassword: yup.string().required('Current Password is Required'),
    newPassword: yup
      .string()
      .required('New Password is Required')
      .matches(
        /^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*]).{8,}$/,
        'Password must contain numbers, uppercase letters, lowercase letters, and special characters, and has minimum length is 8'
      )
      .notOneOf([yup.ref('currentPassword')], 'New password must be different current password'),
    rePassword: yup
      .string()
      .oneOf([yup.ref('newPassword'), null], 'Re-password must be same password')
      .required('Repeat New Password is Required')
  })
  const formik = useFormik({
    initialValues: {
      currentPassword: '',
      newPassword: '',
      rePassword: ''
    },
    validationSchema: schema,
    onSubmit: (values, { resetForm }) => {
      handleChangePassword(values, resetForm)
    }
  })

  const handleChangePassword = async (data, resetForm) => {
    try {
      const request_data = {
        oldPassword: data.currentPassword,
        newPassword: data.newPassword
      }
      const resultAction = await dispatch(changePassword(request_data))
      const result = unwrapResult(resultAction)
      Swal.fire({
        icon: 'success',
        title: `Change Password Success!!!`,
        confirmButtonText: 'Oke'
      }).then((result) => {
        if (result.isConfirmed) {
          resetForm()
        }
      })
    } catch (e) {
      Swal.fire({
        icon: 'error',
        title: 'Change Password Failed!!!',
        text: e
      })
    }
  }

  return (
    <section className='change-password-section box-shadown rounded-5'>
      <h3 className='p-3 text-center'>Change Password</h3>
      <div className='change-password-list'>
        <form onSubmit={formik.handleSubmit}>
          <div className={!(formik.touched.currentPassword && formik.errors.currentPassword) ? 'mb-3' : ''}>
            <label htmlFor='current-password' className='form-label'>
              Current Password
            </label>
            <input
              type='password'
              className='form-control'
              id='current-password'
              placeholder='Current Password'
              onChange={formik.handleChange('currentPassword')}
              value={formik.values.currentPassword}
            />
          </div>
          <div className='error mt-1'>{formik.touched.currentPassword && formik.errors.currentPassword}</div>
          <div className={!(formik.touched.newPassword && formik.errors.newPassword) ? 'mb-3' : ''}>
            <label htmlFor='new-password' className='form-label'>
              New Password
            </label>
            <input
              type='password'
              className='form-control'
              id='new-password'
              placeholder='New Password'
              onChange={formik.handleChange('newPassword')}
              value={formik.values.newPassword}
            />
          </div>
          <div className='error mt-1'>{formik.touched.newPassword && formik.errors.newPassword}</div>
          <div className={!(formik.touched.newPassword && formik.errors.newPassword) ? 'mb-3' : ''}>
            <label htmlFor='repeat-new-password' className='form-label'>
              Repeat New Password
            </label>
            <input
              type='password'
              className='form-control'
              id='repeat-new-password'
              placeholder='Repeat New Password'
              onChange={formik.handleChange('rePassword')}
              value={formik.values.rePassword}
            />
          </div>
          <div className='error mt-1'>{formik.touched.rePassword && formik.errors.rePassword}</div>
          <div className='d-flex align-items-center justify-content-center'>
            <button className='button' type='submit'>
              Change Password
            </button>
          </div>
        </form>
      </div>
    </section>
  )
}

export default ChangePasswordCard
