import React from 'react'
import 'react-toastify/dist/ReactToastify.css'
import './style.scss'
function CustomToastContainer({ header, message }) {
  return (
    <div>
      <div className='toast-header'>
        <strong className='mr-auto'>{header}</strong>
      </div>
      <div className='toast-body'>{message}</div>
    </div>
  )
}

export default CustomToastContainer
