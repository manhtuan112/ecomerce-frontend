import React from 'react'
import './style.scss'
import { Link } from 'react-router-dom'

function BlogCard() {
  return (
    <div className='blog-card'>
      <div className='card-image'>
        <img src='https://cdn.tgdd.vn/Files/2016/03/04/796801/anhdaidien.jpg' className='img-fluid' alt='image' />
      </div>
      <div className='blog-content'>
        <p className='date'>1 Dec, 2023</p>
        <h5 className='title'>A Beautiful Sunday Moring Renalsance</h5>
        <p className='desc'>
          Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis ratione maiores aliquam assumenda vero praesentium rem consequuntur neque alias rerum laborum minima similique, tempora et officia minus ullam! Illo, corrupti.
        </p>
        <Link className='button' to='/blog/:id'>
          READ MORE
        </Link>
      </div>
    </div>
  )
}

export default BlogCard
