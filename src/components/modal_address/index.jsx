import React, { useEffect, useState } from 'react'
import Modal from 'react-bootstrap/Modal'
function ModalAddAddress({ modalShow, setModalShow, handleAddNewAddress }) {
  const [listProvince, setListProvince] = useState([{ id: 0, name: 'Select City' }])
  const [listDistrict, setListDistrict] = useState([])
  const [listWard, setListWard] = useState([])
  const [currentCity, setCurrentCity] = useState(0)
  const [currentDistrict, setCurrentDistrict] = useState(0)
  const [currentWard, setCurrentWard] = useState(0)
  const [currentAddress, setCurrentAddress] = useState({ home_number: '' })

  useEffect(() => {
    fetch('https://dev-online-gateway.ghn.vn/shiip/public-api/master-data/province', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        token: 'b4e434ed-ae2f-11ee-8bfa-8a2dda8ec551'
      }
    })
      .then((response) => response.json())
      .then((data) => {
        const province = data.data.map((item) => {
          return {
            id: item.ProvinceID,
            name: item.ProvinceName
          }
        })
        setListProvince([...listProvince, ...province])
      })
      .catch((error) => console.error(error))
  }, [])

  useEffect(() => {
    if (currentCity === 0) return
    fetch('https://dev-online-gateway.ghn.vn/shiip/public-api/master-data/district', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        token: 'b4e434ed-ae2f-11ee-8bfa-8a2dda8ec551'
      },
      body: JSON.stringify({
        province_id: Number(currentCity)
      })
    })
      .then((response) => response.json())
      .then((data) => {
        const district = data.data.map((item) => {
          return {
            id: item.DistrictID,
            name: item.DistrictName
          }
        })
        setListDistrict(district)
      })
      .catch((error) => console.error(error))
  }, [currentCity])

  useEffect(() => {
    if (currentDistrict === 0) return
    fetch('https://dev-online-gateway.ghn.vn/shiip/public-api/master-data/ward?district_id', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        token: 'b4e434ed-ae2f-11ee-8bfa-8a2dda8ec551'
      },
      body: JSON.stringify({
        district_id: Number(currentDistrict)
      })
    })
      .then((response) => response.json())
      .then((data) => {
        const ward = data.data.map((item) => {
          return {
            id: item.WardCode,
            name: item.WardName
          }
        })
        setListWard(ward)
      })
      .catch((error) => console.error(error))
  }, [currentDistrict])

  const handleOnChangeCity = (e) => {
    const city = e.target.value
    setCurrentCity(city)
    const currentCityName = listProvince.find((item) => item.id === Number(city)).name
    setCurrentAddress({ ...currentAddress, city: currentCityName })
  }

  const handleOnChangeDistrict = (e) => {
    const district = e.target.value
    setCurrentDistrict(district)
    const currentDistrictName = listDistrict.find((item) => item.id === Number(district)).name
    setCurrentAddress({ ...currentAddress, district: currentDistrictName })
  }

  const handleOnChangeWard = (e) => {
    const ward = e.target.value
    setCurrentWard(ward)
    const currentWardName = listWard.find((item) => item.id === ward).name
    setCurrentAddress({ ...currentAddress, ward: currentWardName })
  }

  return (
    <Modal
      size='lg'
      aria-labelledby='contained-modal-title-vcenter'
      centered
      show={modalShow}
      onHide={() => setModalShow(false)}
    >
      <Modal.Header closeButton>
        <Modal.Title id='contained-modal-title-vcenter'>Add New Address</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form action='' className='d-flex flex-wrap gap-15 justify-content-between'>
          <div className='w-100'>
            <input
              type='text'
              className='form-control'
              placeholder='Address'
              value={currentAddress.home_number}
              onChange={(e) => setCurrentAddress({ ...currentAddress, home_number: e.target.value })}
            />
          </div>
          <div className='flex-grow-1'>
            <select
              name=''
              id=''
              className='form-control form-select'
              value={currentCity}
              onChange={handleOnChangeCity}
            >
              {listProvince.map((item, index) => (
                <option key={index} value={item.id}>
                  {item.name}
                </option>
              ))}
            </select>
          </div>
          <div className='flex-grow-1'>
            <select
              name=''
              id=''
              className='form-control form-select'
              value={currentDistrict.id}
              onChange={handleOnChangeDistrict}
            >
              {listDistrict.length === 0 ? (
                <option value='' selected disabled>
                  Select District
                </option>
              ) : (
                listDistrict.map((item, index) => (
                  <option key={index} value={item.id}>
                    {item.name}
                  </option>
                ))
              )}
            </select>
          </div>
          <div className='flex-grow-1'>
            <select
              name=''
              id=''
              className='form-control form-select'
              value={currentWard.id}
              onChange={handleOnChangeWard}
            >
              {listWard.length === 0 ? (
                <option value='' selected disabled>
                  Select Ward
                </option>
              ) : (
                listWard?.map((item, index) => (
                  <option key={index} value={item.id}>
                    {item.name}
                  </option>
                ))
              )}
            </select>
          </div>
        </form>
      </Modal.Body>
      <Modal.Footer>
        <button onClick={() => handleAddNewAddress(currentAddress)} type='button' className='btn btn-primary'>
          Add
        </button>
        <button onClick={() => setModalShow(false)} type='button' className='btn btn-outline-primary'>
          Close
        </button>
      </Modal.Footer>
    </Modal>
  )
}

export default ModalAddAddress
