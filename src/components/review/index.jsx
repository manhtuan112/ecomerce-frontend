import React from 'react'
import './style.scss'
import ReactStars from 'react-rating-stars-component'
import { convertDateTime } from '../../features/common'
import { IoMdStar } from 'react-icons/io'

function Review({ data }) {
  return (
    <div className='review mt-5'>
      <div className='review-info d-flex align-items-center gap-15'>
        <div className='review-avatar'>
          <div>
            <img src={data.avatar} alt='avatar' />
          </div>
        </div>
        <div className='review-person-info'>
          <div className='d-flex flex-column'>
            <div className='username'>{data.username}</div>
            <div className='rating-and-date d-flex align-items-center gap-30'>
              <div>
                {[1, 2, 3, 4, 5].map((item, index) => {
                  if (item <= data.starRating) return <IoMdStar key={index} className='fs-5' color='#FFD700' />
                  else return <IoMdStar key={index} className='fs-5 m' color='#808080' />
                })}
              </div>

              <span className='date'>{convertDateTime(data.created_at)}</span>
            </div>
          </div>
        </div>
      </div>
      <div className='review-content mt-3'>
        <p>{data.content}</p>
      </div>
    </div>
  )
}

export default Review
