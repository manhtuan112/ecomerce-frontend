import React from 'react'

function SectionWrappep({className, children, noPadding}) {
  return (
    <section className={`${className} ${noPadding ? '' : 'py-5'}`}>
      <div className='container-xxl'>
        <div className='row'>
          {children}
        </div>
      </div>
    </section>
  )
}

export default SectionWrappep
