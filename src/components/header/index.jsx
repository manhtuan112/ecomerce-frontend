import React, { useEffect, useState } from 'react'
import { Link, NavLink } from 'react-router-dom'
import { BsSearch } from 'react-icons/bs'
import { FaCodeCompare, FaCartShopping } from 'react-icons/fa6'
import { FaRegHeart } from 'react-icons/fa'
import { LuUser } from 'react-icons/lu'
import { CgMenuGridO } from 'react-icons/cg'
import { useDispatch, useSelector } from 'react-redux'
import { CgProfile } from 'react-icons/cg'
import { IoIosLogOut } from 'react-icons/io'
import { useNavigate } from 'react-router-dom'

import './style.scss'
import { getLocalStorage, removeLocalStorage } from '../../utils/localStorage'
import InitialIcon from '../initialIcon'
import { resetState } from '../../features/authenticate/userSlice'
import Swal from 'sweetalert2'
import { getGeneralUserInfoMethod } from '../../features/authenticate/middleware'
import Loading from '../../pages/loading'
import { Typeahead } from 'react-bootstrap-typeahead'
import 'react-bootstrap-typeahead/css/Typeahead.css'

function Header() {
  const { numberCart } = useSelector((state) => state.cart)
  const user = useSelector((state) => state.user)
  const { list_product } = useSelector((state) => state?.product)
  const [list, setList] = useState([])
  const dispatch = useDispatch()
  const nav = useNavigate()
  const [paginate, setPaginate] = useState(true)

  useEffect(() => {
    if (getLocalStorage('accessToken')) {
      dispatch(getGeneralUserInfoMethod())
    }
  }, [])

  useEffect(() => {
    if (list_product) {
      setList(list_product)
    } else{
      setList([])
    }
  }, [list_product])

  const handleLogout = () => {
    Swal.fire({
      icon: 'question',
      title: 'Do you want logout?',
      showDenyButton: true,
      confirmButtonText: 'Yes',
      denyButtonText: `No`
    }).then((result) => {
      if (result.isConfirmed) {
        removeLocalStorage('accessToken')
        dispatch(resetState())
        nav('/')
      } else if (result.isDenied) {
      }
    })
  }
  return (
    <>
      {user.isLogin === 'pending' && <Loading />}
      <header className='header-top-strip py-3'>
        <div className='container-xxl'>
          <div className='row'>
            <div className='col-6'>
              <p className='text-white mb-0'>Lorem ipsum dolor sit amet consectetur adipisicing elit</p>
            </div>
            <div className='col-6'>
              <p className='text-end text-white mb-0'>
                {' '}
                Hotline: <Link to='tel: +85 38864256'>0388864256</Link>
              </p>
            </div>
          </div>
        </div>
      </header>
      <header className='header-upper py-3'>
        <div className='container-xxl'>
          <div className='row align-items-center'>
            <div className='col-2'>
              <NavLink className='text-white' to='/'>
                {' '}
                <img
                  src='https://firebasestorage.googleapis.com/v0/b/uploadimagetest-d03e3.appspot.com/o/image%2FOrange%20White%20Bold%20Illustrative%20Fashion%20Logo.png?alt=media&token=4e8b5f92-329f-4359-b76e-7a978a769c42'
                  className='image-logo'
                />
              </NavLink>
            </div>
            <div className='col-5'>
              <div className='input-group'>
                <Typeahead
                  id='pagination-example'
                  onPaginate={() => {}}
                  options={list}
                  onChange={(selected) => {
                    if (selected.length === 0) {
                    } else {
                      nav(`/product/${selected[0].id}`)
                    }
                  }}
                  paginate={paginate}
                  placeholder='Search for Products here...'
                  labelKey={'title'}
                />
                <span className='input-group-text p-3' id='basic-addon2'>
                  <BsSearch className='fs-6' />
                </span>
              </div>
            </div>
            <div className='col-5'>
              <div className='header-upper-links d-flex align-items-center justify-content-between'>
                <div>
                  <NavLink to='/compare-product' className='d-flex align-items-center gap-10 text-white'>
                    <FaCodeCompare className='fs-3' />
                    <p className='mb-0'>
                      Compare <br /> Product
                    </p>
                  </NavLink>
                </div>
                <div>
                  <NavLink to='/wishlist' className='d-flex align-items-center gap-10 text-white'>
                    <FaRegHeart className='fs-3' />
                    <p className='mb-0'>
                      Favourite <br /> wishlist
                    </p>
                  </NavLink>
                </div>
                <div>
                  {user.isLogin === 'success' ? (
                    <div className='dropdown no-dropdown-toggle'>
                      <button
                        className='btn btn-secondary dropdown-toggle bg-transparent border-0 gap-15 d-flex align-items-center p-0'
                        type='button'
                        id='dropdownMenuButton1'
                        data-bs-toggle='dropdown'
                        aria-expanded='false'
                      >
                        <InitialIcon letter={user.username.charAt(0)} src={user.avatar} />
                        <span className='d-inline-block'>{user.username}</span>
                      </button>
                      <ul className='dropdown-menu' aria-labelledby='dropdownMenuButton1'>
                        <li>
                          <div className='d-flex justify-content-between align-items-center'>
                            <CgProfile className='text-white fs-3 ms-2' />
                            <Link className='dropdown-item text-white' to='/user-information'>
                              My Account
                            </Link>
                          </div>
                        </li>
                        <li>
                          <div className='d-flex justify-content-between align-items-center'>
                            <IoIosLogOut className='text-white fs-3 ms-2' />
                            <button className='dropdown-item text-white text-capitalize' onClick={handleLogout}>
                              Logout
                            </button>
                          </div>
                        </li>
                      </ul>
                    </div>
                  ) : (
                    <NavLink to='/login' className='d-flex align-items-center gap-10 text-white'>
                      <LuUser className='fs-3' />
                      <p className='mb-0'>Login</p>
                    </NavLink>
                  )}
                </div>
                <div>
                  <NavLink to='/cart' className='text-white position-relative'>
                    <FaCartShopping className='fs-3' />
                    {user.isLogin === 'success' && (
                      <div className='position-absolute number-cart'>
                        <span className='badge'>{numberCart === 0 ? getLocalStorage('numberCart') : numberCart}</span>
                      </div>
                    )}
                  </NavLink>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
      <header className='header-bottom py-3'>
        <div className='container-xxl'>
          <div className='row'>
            <div className='col-12'>
              <div className='menu-bottom d-flex align-items-center gap-30'>
                <div>
                  <div className='dropdown'>
                    <button
                      className='btn btn-secondary dropdown-toggle bg-transparent border-0 gap-15 d-flex align-items-center'
                      type='button'
                      id='dropdownMenuButton1'
                      data-bs-toggle='dropdown'
                      aria-expanded='false'
                    >
                      <CgMenuGridO className='fs-4' />
                      <span className='me-5 d-inline-block'>Show categories</span>
                    </button>
                    <ul className='dropdown-menu' aria-labelledby='dropdownMenuButton1'>
                      <li>
                        <Link className='dropdown-item text-white' to=''>
                          Action
                        </Link>
                      </li>
                      <li>
                        <Link className='dropdown-item text-white' to=''>
                          Another action
                        </Link>
                      </li>
                      <li>
                        <Link className='dropdown-item text-white' to=''>
                          Something else here
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className='menu-links'>
                  <div className='d-flex align-items-center gap-15'>
                    <NavLink to='/'>Home</NavLink>
                    <NavLink to='/store'>Our store</NavLink>
                    <NavLink to='/blogs'>Blogs</NavLink>
                    <NavLink to='/contact'>Contact</NavLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
    </>
  )
}

export default Header
