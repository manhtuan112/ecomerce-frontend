import React from 'react'
import SectionWrappep from '../section_wrapper'
import { Link } from 'react-router-dom'

function BreadCrumb(props) {
  const { title } = props
  return (
    <SectionWrappep className='breadcrumb mb-0'>
      <div className='col-12'>
        <p className='text-center mb-0'>
          <Link to='/' className='text-dark'>
            Home
          </Link>{' '}
          / {title}
        </p>
      </div>
    </SectionWrappep>
  )
}

export default BreadCrumb
