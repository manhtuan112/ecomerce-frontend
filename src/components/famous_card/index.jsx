import React, { useEffect, useState } from 'react'
import './style.scss'
import { removeDescription } from '../../features/common'
import { Link } from 'react-router-dom'
function FamousCard({ data, list_category }) {
  const [category_name, setCategory_name] = useState()
  useEffect(() => {
    const found = list_category.find((element) => element.id === data.category)
    setCategory_name(found?.name)
  }, [])

  return (
    <Link className='col-3' to={`product/${data.id}`}>
      <div className='famous-card position-relative'>
        <div className='overlay color-red'></div>
        <div className='famous-card-image'>
          <img
            src={
              data.image
                ? data.image
                : 'https://media.licdn.com/dms/image/D4E12AQFAx030aGgdSQ/article-cover_image-shrink_720_1280/0/1693505218173?e=2147483647&v=beta&t=YC1XVE2ljS94xV45ijOh3BuguQJtw7d6Eoxet4JQuy0'
            }
            alt='picture'
            className='img-fluid'
          />
        </div>
        <div className='famous-content position-absolute'>
          <div>
            <h5>{category_name ? category_name : 'No Category'}</h5>
            <h6>{data.title}</h6>
            <p> {removeDescription(data.description, 200)}</p>
          </div>

          <h6>Quantity sold: {data.quantity_sold}</h6>
        </div>
      </div>
    </Link>
  )
}

export default FamousCard
