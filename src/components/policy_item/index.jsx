import React from 'react'

function PolicyItem() {
  return (
    <div className='policy-session'>
      <h5 className='header'>Lorem ipsum dolor sit amet consectetur.</h5>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero tenetur tempore placeat velit quae vel
        dignissimos perferendis non magni earum eveniet corrupti, modi laboriosam et ut voluptatem obcaecati illo.
        Consectetur hic quisquam sunt vitae. Itaque minus aut velit dolorem enim officiis, excepturi doloribus debitis
        eum laborum dicta consequuntur? Animi sunt quidem voluptatem id at, vel suscipit cum facere sint numquam.
      </p>
    </div>
  )
}

export default PolicyItem
