import React from 'react'
import ReactStars from 'react-rating-stars-component'
import { CiHeart } from 'react-icons/ci'
import { VscGitCompare } from 'react-icons/vsc'
import { IoEyeOutline } from 'react-icons/io5'
import { IoBagOutline } from 'react-icons/io5'
import { ImCancelCircle } from 'react-icons/im'
import './style.scss'
import { Link, useLocation } from 'react-router-dom'
import { convertMoneyFormat, removeDescription } from '../../features/common'

function ProductCard(props) {
  const { grid, data } = props
  let location = useLocation().pathname
  return (
    <div className={`${location == '/store' ? `grid-${grid}` : 'col-3'} mb-3`}>
      <Link to={`/product/${data.id}`} className='product-card h-100 w-100 position-relative'>
        {data.discount && data.discount.status === true && (
          <div className='discount position-absolute'>
            <span className='sale'>{Math.round(data.discount.discount_percent)}%</span>
          </div>
        )}

        <div className='wishlist-icon position-absolute'>
          {location === '/wishlist' ? (
            <ImCancelCircle
              className='position-absolute cross fs-5'
              style={{ top: '0px', right: '0px', color: 'black' }}
            />
          ) : (
            // show cancel button when in wishlist
            <CiHeart color='#000' size={20} />
          )}
        </div>
        <div className='product-image mb-4 d-flex align-items-center justify-content-center'>
          <img src={data.image} alt='product-image' className='img-fluid' />
        </div>
        <div className='product-details'>
          <h6 className='brand'>Iphone</h6>
          <h5 className='product-title'>{data.title}</h5>
          {/* show star rating if not in wishlist page */}
          {location !== '/wishlist' && (
            <ReactStars count={5} size={24} value={parseFloat(data.averageRating)} edit={false} activeColor='#ffd700' />
          )}
          <p className={`product-description ${grid === 12 ? 'd-block' : 'd-none'}`}>
            {removeDescription(data.description, 300)}
          </p>
          <p
            className={`price ${
              data.discount && data.discount.status === true && 'active'
            } d-flex justify-content-between align-items-center`}
          >
            <span>{convertMoneyFormat(data.price)} VND</span>
            {data.status === 'Out of Stock' && <span className='sold_out'>Sold out</span>}
          </p>
          {data.discount && data.discount.status === true && (
            <p className='discount-price'>
              {convertMoneyFormat(Math.round((data.price * (100 - data.discount.discount_percent)) / 100))} VND
            </p>
          )}
        </div>
        {/* show action bar if not in wishlist page */}
        {location !== '/wishlist' && (
          <div className='action-bar position-absolute'>
            <div className='d-flex flex-column gap-15'>
              <Link>
                <VscGitCompare color='#000' size={18} />
              </Link>
              <Link>
                <IoEyeOutline color='#000' size={18} />
              </Link>
              <Link>
                <IoBagOutline color='#000' size={18} />
              </Link>
            </div>
          </div>
        )}
      </Link>
    </div>
  )
}

export default ProductCard
