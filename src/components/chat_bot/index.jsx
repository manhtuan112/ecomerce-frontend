import React, { useEffect, useRef, useState } from 'react'
import '@chatscope/chat-ui-kit-styles/dist/default/styles.min.css'
import {
  MainContainer,
  ChatContainer,
  MessageList,
  Message,
  MessageInput,
  ConversationHeader,
  Avatar,
  TypingIndicator,
  VoiceCallButton,
  VideoCallButton,
  InfoButton,
  Status
} from '@chatscope/chat-ui-kit-react'
import { useDispatch, useSelector } from 'react-redux'
import { addMessageToList } from '../../features/chatbot/chatbotSlice'
import { chatbotMethod } from '../../features/chatbot/middleware'
import { unwrapResult } from '@reduxjs/toolkit'

function Chatbot() {
  const systemMessage = {
    role: 'system',
    content: 'You are an electronics expert. Feel free to ask any questions or seek advice related to electronics' // This is the system message that defines the logic of our chatGPT
  }
  const inputRef = useRef('')
  const [typing, setTyping] = useState(false)
  const [isIntroductUserNotLogin, setIsIntroductUserNotLogin] = useState(false)

  const {isLogin} = useSelector((state) => state.user)
  const messages = useSelector((state) => state.chatbot.listChatbot)
  const dispatch = useDispatch()
  useEffect(() => {
    if (isLogin !== 'success' && !isIntroductUserNotLogin) {
      dispatch(addMessageToList({ message: "Looks like you're not logged into our system. Please log in so we can advise you on the best products", sender: 'Chatbot', introduct: 'not_login' }))
      setIsIntroductUserNotLogin(true)
    }
    if(isLogin === 'success' && isIntroductUserNotLogin){
      dispatch(addMessageToList({ message: "Welcome back, we're glad you're back. But Looks like you haven't rated any products on the system yet. Let me know which product category you are interested in", sender: 'Chatbot'}))
    }

  }, [isLogin])
  

  const handleSend = async (message) => {
    const newMessage = message.replace(/^(&nbsp;)+|(&nbsp;)+$/g, '').trim() // remove white space
    if (newMessage === '') return
    const newMessageObject = {
      message: newMessage,
      sender: 'user',
      direction: 'outgoing'
    }
    const newMessages = [...messages, newMessageObject]
    dispatch(addMessageToList(newMessageObject))
    setTyping(true)
    if (newMessage === 'I like Mobile Phone'){
      dispatch(addMessageToList({ message: "Great our store currently has a number of highly rated products in this category ", sender: 'Chatbot'}))
      dispatch(addMessageToList({ message: "These products are displayed in the Suggestion for you section", sender: 'Chatbot'}))
    }
    await processMessageToChatGPT(newMessages)
  }

  async function processMessageToChatGPT(chatMessages) {
    // messages is an array of messages
    // Format messages for chatGPT API
    // API is expecting objects in format of { role: "user" or "assistant", "content": "message here"}

    let apiMessages = chatMessages.map((messageObject) => {
      let role = ''
      if (messageObject.sender === 'Chatbot') {
        role = 'assistant'
      } else {
        role = 'user'
      }
      return { role: role, content: messageObject.message }
    })

    // Get the request body set up with the model we plan to use
    // and the messages which we formatted above. We add a system message in the front to'
    // determine how we want chatGPT to act.
    const apiRequestBody = {
      model: 'gpt-3.5-turbo',
      messages: [
        systemMessage, // The system message DEFINES the logic of our chatGPT
        ...apiMessages // The messages from our chat with ChatGPT
      ]
    }

    const resultAction = await dispatch(chatbotMethod(apiRequestBody))
    try {
      const result = unwrapResult(resultAction)
      console.log(result.choices[0].message.content)
      dispatch(addMessageToList({ message: result.choices[0].message.content, sender: 'Chatbot' }))
    } catch (e) {
      console.log(e)
    }

    setTyping(false)
    inputRef.current.focus()
  }
  return (
    <div
      className='box-shadow rouded-5'
      style={{
        position: 'fixed',
        bottom: '0',
        left: 'calc(80%)',
        height: '450px',
        width: '20%',
        zIndex: '999',
        overflow: 'hidden'
      }}
    >
      <MainContainer>
        <ChatContainer>
          <ConversationHeader>
            <Avatar
              name=''
              src='https://media.istockphoto.com/id/1010001882/vector/%C3%B0%C3%B0%C2%B5%C3%B1%C3%B0%C3%B1%C3%B1.jpg?s=612x612&w=0&k=20&c=1jeAr9KSx3sG7SKxUPR_j8WPSZq_NIKL0P-MA4F1xRw='
            />
            <ConversationHeader.Content
              userName='Chatbot'
              info={
                <div className='d-flex align-items-center gap-10'>
                  <Status status='available' /> <span>Available</span>
                </div>
              }
            />
            <ConversationHeader.Actions>
              <VoiceCallButton />
              <VideoCallButton />
              <InfoButton />
            </ConversationHeader.Actions>
          </ConversationHeader>
          <MessageList
            typingIndicator={
              typing ? <TypingIndicator content={'Chatbot is typing'} style={{ height: '35px' }} /> : null
            }
            style={{ marginTop: '15px', paddingBottom: '20px' }}
          >
            {messages.map((message, index) => {
              if (message.sender === 'Chatbot') {
                return (
                  <Message key={index} model={message} style={{ marginBottom: '15px' }}>
                    <Avatar
                      src={
                        'https://media.istockphoto.com/id/1010001882/vector/%C3%B0%C3%B0%C2%B5%C3%B1%C3%B0%C3%B1%C3%B1.jpg?s=612x612&w=0&k=20&c=1jeAr9KSx3sG7SKxUPR_j8WPSZq_NIKL0P-MA4F1xRw='
                      }
                      name={'Chatbot'}
                    />
                  </Message>
                )
              }
              return <Message key={index} model={message} style={{ marginBottom: '15px' }}></Message>
            })}
          </MessageList>

          <MessageInput placeholder='Type message here' onSend={handleSend} ref={inputRef} />
        </ChatContainer>
      </MainContainer>
    </div>
  )
}

export default Chatbot
