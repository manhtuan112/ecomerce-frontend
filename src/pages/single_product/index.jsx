import React, { useEffect, useState } from 'react'
import Meta from '../../components/meta'
import BreadCrumb from '../../components/bread_crumb'
import SectionWrappep from '../../components/section_wrapper'
import ProductCard from '../../components/product_card'
import ReactStars from 'react-rating-stars-component'
import './style.scss'
import Review from '../../components/review'
import Color from '../../components/color'
import { IoIosGitCompare } from 'react-icons/io'
import { CiHeart } from 'react-icons/ci'
import { LiaShippingFastSolid } from 'react-icons/lia'
import { GiMaterialsScience } from 'react-icons/gi'
import { useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import {
  getDetailProduct,
  getListProductMethod,
  updateRatingStartForProductMethod
} from '../../features/product/middleware'
import Loading from '../loading'
import { createReviewMethod, getListReiviewByProductMethod } from '../../features/review/middleware'
import { convertMoneyFormat } from '../../features/common'
import { toast, ToastContainer } from 'react-toastify'
import CustomToastContainer from '../../components/CustomToastContainer'
import { unwrapResult } from '@reduxjs/toolkit'
import { addToCartMethod, getListCartMethod } from '../../features/cart/middleware'

function SingleProduct() {
  const { id } = useParams()
  const dispatch = useDispatch()
  const { avarage_rating, Reviews } = useSelector((state) => state.review.list_review)
  const { recommendProductList } = useSelector((state) => state.review)
  const { current_product, list_product } = useSelector((state) => state.product)
  const { isLoading: productStatusLoading } = useSelector((state) => state.product.status.isLoading)
  const { isLoading: reviewLoading } = useSelector((state) => state.review.status.isLoading)
  const { username, avatar, isLogin } = useSelector((state) => state.user)
  const [colors, setColors] = useState()
  const [sizes, setSizes] = useState()
  const [variantValue, setVariantValue] = useState(['', '', ''])
  const [numberProduct, setNumberProduct] = useState(1)
  const [isSizeActive, setIsSizeActive] = useState('')
  const [isColorActive, setIsColorActive] = useState('1')
  const [quantity, setQuantity] = useState(null)
  const [variantChooseId, setVariantChooseId] = useState() // add to card or buy now
  const [showWriteCommend, setShowWriteCommend] = useState(false)

  const [newCommendInfo, setNewCommendInfo] = useState({ startRating: 0, content: '' })
  const [recommedListByCategory, setRecommedListByCategory] = useState([])

  useEffect(() => {
    if (!list_product) {
      dispatch(getListProductMethod())
    }
    if (current_product) {
      if (parseInt(id) !== current_product.id) {
        dispatch(getDetailProduct(id))
        dispatch(getListReiviewByProductMethod(id))
      }
    } else {
      dispatch(getDetailProduct(id))
      dispatch(getListReiviewByProductMethod(id))
    }
  }, [id])

  useEffect(() => {
    // generate list color and size
    const handleGetRecommentList = () => {
      let list = recommendProductList.filter(
        (item, index) => item.category === current_product.category?.id && item.id !== current_product?.id
      )
      if (list.length <= 5) {
        list = list_product.filter(
          (item, index) => item.category === current_product?.category.id && item.id !== current_product?.id
        )
        list.sort((a, b) => b.averageRating - a.averageRating)
      }
      setRecommedListByCategory(list)
    }
    if (current_product) {
      if (current_product.variant.length > 0) {
        const colors = [...new Set(current_product.variant.map((item) => item.color.color_code))]
        const size = [...new Set(current_product.variant.map((item) => `${item.size.size_num} ${item.size.size_unit}`))]
        setColors(colors)
        setSizes(size)
      } else {
        setColors([])
        setSizes([])
      }
      if (current_product.variant.length > 0) {
        const colors = [...new Set(current_product.variant.map((item) => item.color.color_code))]
        setColors(colors)
      } else {
        setColors([])
      }
      handleGetRecommentList()
    }
  }, [current_product])

  useEffect(() => {
    let status = true
    for (let item of variantValue) {
      if (item === '') {
        status = false
        break
      }
    }
    if (status) {
      const filteredData = current_product?.variant.filter(
        (item) =>
          item.size.size_num === variantValue[0] &&
          item.size.size_unit === variantValue[1] &&
          item.color.color_code === variantValue[2]
      )
      if (filteredData.length !== 0) {
        setQuantity(filteredData[0].inventory)
        setVariantChooseId(filteredData[0].id)
      } else {
        setQuantity(false)
      }
    }
  }, [variantValue])

  const handleChooseSize = (value) => {
    const result = value.split(' ')
    const arr = [...variantValue]
    arr[0] = parseInt(result[0])
    arr[1] = result[1]
    setVariantValue(arr)
    setIsSizeActive(value)
  }

  const handleChooseColor = (value) => {
    const arr = [...variantValue]
    arr[2] = value
    setVariantValue(arr)
    setIsColorActive(value)
  }

  const handleShowWriteCommend = () => {
    if (isLogin !== 'success') {
      toast.error(<CustomToastContainer header='Error' message='Please login to write a comment' />, {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light'
      })
    } else {
      setShowWriteCommend(true)
    }
  }

  const handleSubmitCommend = async (e) => {
    e.preventDefault()
    let data_avatar = 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png'
    if (avatar !== null) {
      data_avatar = avatar
    }
    const data = {
      avatar: data_avatar,
      username: username,
      product_id: current_product.id,
      content: newCommendInfo.content,
      starRating: newCommendInfo.startRating
    }
    try {
      let resultAction = await dispatch(createReviewMethod(data))
      let result = unwrapResult(resultAction)
      toast.success(<CustomToastContainer header='Success' message='Create Comment Success' />, {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light'
      })
      resultAction = await dispatch(getListReiviewByProductMethod(id))
      result = unwrapResult(resultAction)
      const averageRating = result.data.avarage_rating

      resultAction = await dispatch(
        updateRatingStartForProductMethod({ data: { averageRating: averageRating }, id: id })
      )
      result = unwrapResult(resultAction)

      resultAction = await dispatch(getListProductMethod())
      result = unwrapResult(resultAction)
      setShowWriteCommend(false)
      setNewCommendInfo({ ...newCommendInfo, startRating: 0, content: '' })
    } catch (e) {
      toast.error(<CustomToastContainer header='Create Comment Failed' message='Loi' />, {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light'
      })
    }
  }

  const handleAddToCart = async () => {
    if (isLogin !== 'success') {
      toast.error(<CustomToastContainer header='Error' message='Please login to add to cart' />, {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light'
      })
    } else {
      try {
        const data = {
          quantity: numberProduct,
          color: variantValue[2],
          size: `${variantValue[0]} ${variantValue[1]}`,
          username: username,
          product_id: id
        }
        let resultAction = await dispatch(addToCartMethod(data))
        let result = unwrapResult(resultAction)

        resultAction = await dispatch(getListCartMethod(username))
        result = unwrapResult(resultAction)
        toast.success(<CustomToastContainer header='Success' message='Add to Cart Success' />, {
          position: 'bottom-center',
          autoClose: 3000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
          theme: 'light'
        })
      } catch (e) {
        toast.error(<CustomToastContainer header='Add To Cart Failed' message={e} />, {
          position: 'top-right',
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: 'light'
        })
      }
    }
  }

  return (
    <>
      {(productStatusLoading || reviewLoading) && <Loading />}
      {current_product && (
        <>
          <Meta title={current_product.title} />
          <BreadCrumb title={current_product.title} />
          <ToastContainer />
          <SectionWrappep className='main-product-wrapper home-wrapper-2'>
            <div className='col-6'>
              {current_product.image.length === 0 ? (
                <div className='main-product-image'>
                  <div>
                    <img
                      src='https://www.freeiconspng.com/thumbs/no-image-icon/no-image-icon-4.png'
                      alt='image'
                      className='img-fluid'
                    />
                  </div>
                </div>
              ) : (
                <React.Fragment>
                  <div className='main-product-image'>
                    <div className='d-flex align-items-center'>
                      <img src={current_product.image[0].url} alt='image' className='img-fluid' />
                    </div>
                  </div>

                  <div className='other-product-images d-flex flex-wrap justify-content-center gap-15'>
                    {current_product.image.length > 0 &&
                      current_product.image.map((item, index) => (
                        <div key={index} className='d-flex align-items-center'>
                          <img src={item.url} alt='image' className='img-fluid' />
                        </div>
                      ))}
                  </div>
                </React.Fragment>
              )}
            </div>
            <div className='col-6'>
              <div className='main-product-details'>
                <div className='border-bottom'>
                  <h3 className='title'>{current_product.title}</h3>
                </div>
                <div className='border-bottom py-3'>
                  <p className='price'>{convertMoneyFormat(current_product.price)} VND</p>
                  <div className='d-flex gap-10 align-items-center'>
                    {Math.round(avarage_rating) ? (
                      <ReactStars
                        count={5}
                        size={24}
                        value={Math.round(avarage_rating)}
                        edit={false}
                        activeColor='#ffd700'
                      />
                    ) : (
                      ''
                    )}

                    {Math.round(avarage_rating) === 0 ? (
                      <ReactStars count={5} size={24} value={0} edit={false} activeColor='#ffd700' />
                    ) : (
                      ''
                    )}

                    <p className='mb-0 t-review'>( {Reviews.length} reviews ) </p>
                  </div>
                  <a className='review-btn' href='#review'>
                    View Review
                  </a>
                </div>
                <div className='border-bottom pb-3'>
                  <div className='d-flex gap-10 align-items-center my-2'>
                    <h3 className='product-heading'>Brand: </h3>
                    <p className='product-data'>
                      {current_product.brand?.name ? current_product.brand.name : 'No brand'}
                    </p>
                  </div>
                  <div className='d-flex gap-10 align-items-center my-2'>
                    <h3 className='product-heading'>Category: </h3>
                    <p className='product-data'>
                      {current_product.category?.name ? current_product.brand.name : 'No category'}
                    </p>
                  </div>
                  <div className='d-flex gap-10 align-items-center my-2'>
                    <h3 className='product-heading'>Availablity: </h3>
                    <p className='product-data'>{current_product.status}</p>
                  </div>
                  <div className='d-flex gap-10 flex-column my-2'>
                    <h3 className='product-heading'>Size: </h3>
                    <div className='d-flex flex-wrap gap-15 size-wrapper'>
                      {sizes &&
                        sizes.map((item, index) => (
                          <span
                            className={`badge border border-1 bg-white text-dark border-secondary ${
                              isSizeActive === item ? 'size-active' : ''
                            }`}
                            key={index}
                            onClick={() => handleChooseSize(item)}
                          >
                            {item}
                          </span>
                        ))}
                    </div>
                  </div>
                  {colors && (
                    <div className='d-flex gap-10 flex-column my-2'>
                      <h3 className='product-heading'>Color: </h3>
                      <Color data={colors} handleChooseColor={handleChooseColor} isColorActive={isColorActive} />
                    </div>
                  )}
                  {quantity && (
                    <div className='d-flex gap-10 flex-column my-2'>
                      <h3 className='product-heading'>Quantity: </h3>
                      <div className='product-data d-flex align-items-center gap-15'>
                        <input
                          type='number'
                          name=''
                          min={1}
                          max={10}
                          value={numberProduct}
                          className='form-control'
                          style={{ width: '70px' }}
                          onChange={(e) => setNumberProduct(e.target.value)}
                        />
                        <p className='mb-0 text-dark fs-6'>{quantity} products available</p>
                      </div>
                    </div>
                  )}

                  <div className='d-flex align-items-center gap-10 mt-3 mb-2'>
                    <button className='button border-0 w-50' onClick={handleAddToCart}>
                      Add To Card
                    </button>
                    <button className='button signup border-0 w-50'>Buy Now</button>
                  </div>
                  <div className='d-flex align-items-center gap-15 mt-3 mb-2'>
                    <div>
                      <span>
                        <IoIosGitCompare /> Add to Compare
                      </span>
                    </div>
                    <div>
                      <span>
                        <CiHeart /> Add to Wishlist
                      </span>
                    </div>
                  </div>
                </div>
                <div className='mt-3'>
                  <h3 className='title text-center'>Policy</h3>
                  <div className='accordion' id='accordionPanelsStayOpenExample'>
                    <div className='accordion-item'>
                      <h2 className='accordion-header' id='panelsStayOpen-headingOne'>
                        <button
                          className='accordion-button'
                          type='button'
                          data-bs-toggle='collapse'
                          data-bs-target='#panelsStayOpen-collapseOne'
                          aria-expanded='false'
                          aria-controls='panelsStayOpen-collapseOne'
                        >
                          <div className='d-flex align-items-center gap-10'>
                            <LiaShippingFastSolid /> Shipping & Returns
                          </div>
                        </button>
                      </h2>
                      <div
                        id='panelsStayOpen-collapseOne'
                        className='accordion-collapse collapse show'
                        aria-labelledby='panelsStayOpen-headingOne'
                      >
                        <div className='accordion-body'>
                          The policy should include information about the costs associated with shipping, including any
                          flat rates, shipping fees, or free shipping thresholds. It is important to clearly communicate
                          how shipping costs are calculated and whether they are based on weight, distance, or other
                          factors. <br />
                          he policy may provide estimates or guidelines regarding the expected delivery timeframes for
                          different shipping methods. This can help customers understand when they can expect their
                          orders to arrive.
                        </div>
                      </div>
                    </div>
                    <div className='accordion-item'>
                      <h2 className='accordion-header' id='panelsStayOpen-headingTwo'>
                        <button
                          className='accordion-button collapsed'
                          type='button'
                          data-bs-toggle='collapse'
                          data-bs-target='#panelsStayOpen-collapseTwo'
                          aria-expanded='false'
                          aria-controls='panelsStayOpen-collapseTwo'
                        >
                          <div className='d-flex align-items-center gap-10'>
                            <GiMaterialsScience /> Materials
                          </div>
                        </button>
                      </h2>
                      <div
                        id='panelsStayOpen-collapseTwo'
                        className='accordion-collapse collapse'
                        aria-labelledby='panelsStayOpen-headingTwo'
                      >
                        <div className='accordion-body'>Use environmentally friendly materials</div>
                      </div>
                    </div>
                    <div className='accordion-item'>
                      <h2 className='accordion-header' id='panelsStayOpen-headingThree'>
                        <button
                          className='accordion-button collapsed'
                          type='button'
                          data-bs-toggle='collapse'
                          data-bs-target='#panelsStayOpen-collapseThree'
                          aria-expanded='false'
                          aria-controls='panelsStayOpen-collapseThree'
                        >
                          <div className='d-flex align-items-center gap-10'>
                            <CiHeart /> Customer Protect Policy
                          </div>
                        </button>
                      </h2>
                      <div
                        id='panelsStayOpen-collapseThree'
                        className='accordion-collapse collapse'
                        aria-labelledby='panelsStayOpen-headingThree'
                      >
                        <div className='accordion-body'>
                          <strong>Purchase Terms: </strong>This section outlines the terms and conditions related to the
                          purchase of products or services from the company. It may include information about payment
                          methods, pricing, discounts, and any applicable taxes or fees. <br />
                          <strong>Warranty or Guarantee: </strong> If applicable, this section provides information
                          about any warranties or guarantees offered by the company. It specifies the coverage period,
                          conditions for warranty claims, and how customers can request repairs, replacements, or
                          refunds under warranty.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </SectionWrappep>

          <SectionWrappep className='description-wrapper home-wrapper-2'>
            <div className='col-12'>
              <h4>Description</h4>
              <div className='bg-white p-3'>
                <p>{current_product.description}</p>
              </div>
            </div>
          </SectionWrappep>
          <SectionWrappep className='reviews-wrapper home-wrapper-2'>
            <div className='col-12'>
              <h4>Reviews</h4>
              <div className='review-inner-wrapper'>
                <div className='review-head d-flex justify-content-between align-items-end'>
                  <div>
                    <h4 className='mb-2' id='review'>
                      Customer Reviews
                    </h4>
                    <div className='d-flex gap-10 align-items-center'>
                      <div className='rating-star'>{Number(avarage_rating.toFixed(1))} / 5</div>
                      {Math.round(avarage_rating) ? (
                        <ReactStars
                          count={5}
                          size={24}
                          value={Math.round(avarage_rating)}
                          edit={false}
                          activeColor='#ffd700'
                        />
                      ) : (
                        ''
                      )}

                      {Math.round(avarage_rating) === 0 ? (
                        <ReactStars count={5} size={24} value={0} edit={false} activeColor='#ffd700' />
                      ) : (
                        ''
                      )}
                      <p className='mb-0'>Based on {Reviews.length} reviews</p>
                    </div>
                  </div>

                  <div>
                    <span className='text-decoration-underline' onClick={handleShowWriteCommend}>
                      Write a Review
                    </span>
                  </div>
                </div>
                {showWriteCommend && (
                  <div className='review-form py-4'>
                    <h4>Write a Review</h4>
                    <form onSubmit={handleSubmitCommend} className='d-flex flex-column gap-15'>
                      <div>
                        <ReactStars
                          count={5}
                          size={24}
                          value={newCommendInfo.startRating}
                          edit={true}
                          activeColor='#ffd700'
                          onChange={(e) => setNewCommendInfo({ ...newCommendInfo, startRating: e })}
                        />
                      </div>
                      <div>
                        <textarea
                          name=''
                          id=''
                          className='w-100 form-control'
                          cols={30}
                          rows={4}
                          placeholder='Comments'
                          value={newCommendInfo.content}
                          onChange={(e) => setNewCommendInfo({ ...newCommendInfo, content: e.target.value })}
                        ></textarea>
                      </div>
                      <div className='d-flex justify-content-end'>
                        <button className='button border-0'>Submit Review</button>
                      </div>
                    </form>
                  </div>
                )}

                <div className='reviews'>
                  {Reviews && Reviews.map((item, index) => <Review key={index} data={item} />)}
                </div>
              </div>
            </div>
          </SectionWrappep>
          <SectionWrappep className='popular-wrapper home-wrapper-2'>
            <div className='col-12'>
              <h3 className='section-heading'>Similar products</h3>
            </div>
            {recommedListByCategory && recommedListByCategory.map((item, index) => <ProductCard data={item} />)}
          </SectionWrappep>
        </>
      )}
    </>
  )
}

export default SingleProduct
