import React from 'react'
import Meta from '../../components/meta'
import BreadCrumb from '../../components/bread_crumb'
import SectionWrappep from '../../components/section_wrapper'
import './style.scss'
import { Link } from 'react-router-dom'
import { IoArrowBackCircleOutline } from "react-icons/io5";

function SingleBlog() {
  return (
    <>
      <Meta title='Dynamic Blog Name' />
      <BreadCrumb title='Dynamic Blog Name' />
      <SectionWrappep className='blog-wrapper home-wrapper-2'>
        <div className='col-12'>
          <div className='single-blog-card'>
            <Link to='/blogs' className='d-flex align-items-center gap-10'>
              <IoArrowBackCircleOutline className='fs-4' />
              Go back to Blogs
            </Link>
            <h3 className='title'>Lorem ipsum dolor sit amet.</h3>
            <div>
              <img
                src='https://cdn.tgdd.vn/Files/2016/03/04/796801/anhdaidien.jpg'
                alt='image'
                className='img-fluid w-100 my-4'
              />
            </div>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae nostrum quam tempore ipsa nobis
              deleniti iusto blanditiis at, natus dignissimos magni beatae! Delectus soluta doloribus labore, nulla et
              quasi veritatis molestiae ratione dolorum sint magnam laborum fugit nisi enim aspernatur ea odio molestias
              eveniet quam nihil ipsam earum tempora possimus cumque! Et, accusantium quia! Voluptas expedita quidem,
              repellat deserunt nobis repudiandae pariatur. Delectus explicabo illo velit asperiores cupiditate maiores
              omnis maxime temporibus blanditiis, fugit nulla natus eius? Sint pariatur impedit voluptatum deserunt.
              Consectetur sunt voluptatem esse facere atque quasi aperiam hic fugiat quo, soluta autem dolore possimus
              enim, sequi molestiae?
            </p>
          </div>
        </div>
      </SectionWrappep>
    </>
  )
}

export default SingleBlog
