import React, { useEffect, useState } from 'react'
import Meta from '../../components/meta'
import BreadCrumb from '../../components/bread_crumb'
import SectionWrappep from '../../components/section_wrapper'
import './style.scss'
import CartItem from '../../components/cart_item'
import { Link, useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { deleteCartMethod, getListCartMethod } from '../../features/cart/middleware'
import { toast, ToastContainer } from 'react-toastify'
import CustomToastContainer from '../../components/CustomToastContainer'
import { unwrapResult } from '@reduxjs/toolkit'
import { getProductInfoByCartMethod } from '../../features/product/middleware'
import { convertMoneyFormat } from '../../features/common'
import { createOrderMethod } from '../../features/order/middleware'
import { setListCartDelete } from '../../features/cart/cartSlice'
function Cart() {
  const [listCard, setlistCard] = useState([])
  const [listCardProduct, setListCardProduct] = useState([]) // show cart in minitor not change listCard
  const { username } = useSelector((state) => state.user)
  const { numberCart } = useSelector((state) => state.cart)
  const [subTotalPrice, setSubTotalPrice] = useState(0)
  const dispatch = useDispatch()
  const nav = useNavigate()
  useEffect(() => {
    const fetchData = async () => {
      try {
        let resultAction = await dispatch(getListCartMethod(username))
        let result = unwrapResult(resultAction)
        setlistCard(result.data)
      } catch (e) {
        toast.error(<CustomToastContainer header='Failed' message={e} />, {
          position: 'top-right',
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: 'light'
        })
      }
    }
    fetchData()
  }, [numberCart])

  useEffect(() => {
    const a = []
    const b = [...listCard]
    for (const i in listCard) {
      a.push(dispatch(getProductInfoByCartMethod(listCard[i].product_id)))
    }
    let combine = []
    Promise.all(a)
      .then((results) => {
        combine = results.map((item, index) => Object.assign({ statusCheck: false, ...item.payload.data, ...b[index] }))
        setListCardProduct(combine)
      })
      .catch((error) => {
        alert(error)
      })
  }, [listCard, numberCart])

  const handleDeleteCartItem = async (id) => {
    try {
      let resultAction = await dispatch(deleteCartMethod(id))
      let result = unwrapResult(resultAction)
      toast.success(<CustomToastContainer header='Success' message='Delete Cart Successful' />, {
        position: 'bottom-center',
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: 'light'
      })
    } catch (e) {
      toast.error(<CustomToastContainer header='Delete Cart Failed' message={e} />, {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light'
      })
    }
  }

  const handleCreateOrder = async () => {
    if (subTotalPrice === 0) {
      toast.error(<CustomToastContainer header='Error' message='Plase choose cart to checkout' />, {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: 'light'
      })
      return
    }
    let listCartData = listCardProduct.filter((item) => item.statusCheck === true)
    listCartData = listCartData.map(({ id, product_id, color, size, quantity, price, image, title }) => ({
      id,
      title,
      product_id,
      color,
      size,
      quantity,
      price,
      image: image[0].url
    })) // get some field from listCartData to send to API create Order and call API remove cart
    const data = {
      username: username,
      totalPrice: subTotalPrice,
      list_order_item: listCartData
    }
    try {
      // create order
      let resultAction = await dispatch(createOrderMethod(data))
      let result = unwrapResult(resultAction)
      const id = result.data.id
      toast.success(<CustomToastContainer header='Success' message='Create order successful' />, {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: 'light'
      })
      // set list delete cart, after checkout succesfull will delete
      const listIdDeleteCart = listCartData.map((item) => item.id)
      dispatch(setListCartDelete(listIdDeleteCart))
      nav(`/checkout/${id}`)
    } catch (e) {
      toast.error(<CustomToastContainer header='Error' message={e} />, {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: 'light'
      })
    }
  }

  // Sau có thể mở rộng với tăng số lượng sản phẩm
  const handleCheckedCart = (id) => {
    let cartChecked = listCardProduct.find((item) => item.id === id)
    cartChecked.statusCheck = !cartChecked.statusCheck
    let subTotal = listCardProduct.reduce((acc, curr) => {
      // calculate subTotal price
      if (curr.statusCheck === true) {
        return acc + curr.price * curr.quantity
      }
      return acc
    }, 0)

    setListCardProduct([...listCardProduct])
    setSubTotalPrice(subTotal)
  }

  return (
    <>
      <ToastContainer />

      <Meta title='Cart' />
      <BreadCrumb title='Cart' />
      <SectionWrappep className='cart-wrapper home-wrapper-2'>
        <div className='col-12'>
          <div className='cart-header py-3 d-flex justify-content-between align-content-center'>
            <h4 className='cart-col-0'>Status</h4>
            <h4 className='cart-col-1 ms-5'>Product</h4>
            <h4 className='cart-col-2'>Price</h4>
            <h4 className='cart-col-3'>Quantity</h4>
            <h4 className='cart-col-4'>Total</h4>
            <h4 className='cart-col-5'>Action</h4>
          </div>
          {listCardProduct.length === 0 && <div className='text-center my-5'>No have cart</div>}
          {listCardProduct?.map((item, index) => (
            <CartItem
              key={index}
              data={item}
              handleCheckedCart={handleCheckedCart}
              handleDeleteCartItem={handleDeleteCartItem}
            />
          ))}
        </div>
        <div className='col-12 py-2'>
          <div className='d-flex justify-content-between align-items-baseline'>
            <Link to='/store' className='button'>
              Continue To Shopping
            </Link>
            <div className='d-flex flex-column align-items-end'>
              <h4>SubTotal: {convertMoneyFormat(subTotalPrice)} VND</h4>
              <p>Taxed and shipping calculated at checkout</p>
              <button className='button' onClick={handleCreateOrder}>
                Checkout
              </button>
            </div>
          </div>
        </div>
      </SectionWrappep>
    </>
  )
}

export default Cart
