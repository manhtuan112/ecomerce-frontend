import React from 'react'
import Meta from '../../components/meta'
import BreadCrumb from '../../components/bread_crumb'
import SectionWrappep from '../../components/section_wrapper'
import ProductCard from '../../components/product_card'

function Wishlist() {
  return (
    <>
      <Meta title='Wishlist' />
      <BreadCrumb title='Wishlist' />
      <SectionWrappep className='wishlist-wrapper home-wrapper-2'>
        <ProductCard />
        <ProductCard />
        <ProductCard />
        <ProductCard />
        <ProductCard />
        <ProductCard />
      </SectionWrappep>
    </>
  )
}

export default Wishlist
