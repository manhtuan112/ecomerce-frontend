import React from 'react'
import Meta from '../../components/meta'
import BreadCrumb from '../../components/bread_crumb'
import SectionWrappep from '../../components/section_wrapper'
import { FaHome, FaClock } from 'react-icons/fa'
import { IoCall, IoMail } from 'react-icons/io5'
import './style.scss'
import { Link } from 'react-router-dom'

function Contact() {
  return (
    <>
      <Meta title='Contact Us' />
      <BreadCrumb title='Contact Us' />
      <SectionWrappep className='contact-wrapper home-wrapper-2'>
        <div className='col-12'>
          <h3 className='section-heading mb-0'>Our Location</h3>
          <iframe
            src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3725.256967216795!2d105.78514437600724!3d20.982334080655445!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135accc2630981b%3A0x6b3aa31c38905ced!2zMyBOZ8O1IDYgQW8gU2VuLCBQLiBN4buZIExhbywgSMOgIMSQw7RuZywgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1700989847377!5m2!1svi!2s'
            height='450'
            className='border-0 w-100'
            allowFullScreen=''
            loading='lazy'
            referrerPolicy='no-referrer-when-downgrade'
          ></iframe>
        </div>
        <div className='col-12 mt-5'>
          <div className='contact-inner-wrapper d-flex justify-content-between'>
            <div>
              <h3 className='contact-title mb-4'>Contact</h3>
              <form action='' className='d-flex flex-column gap-15'>
                <div>
                  <input type='text' className='form-control' placeholder='Name' />
                </div>
                <div>
                  <input type='email' className='form-control' placeholder='Email' />
                </div>
                <div>
                  <input type='text' className='form-control' placeholder='Mobile Number' />
                </div>
                <div>
                  <textarea
                    name=''
                    id=''
                    className='w-100 form-control'
                    cols={30}
                    rows={4}
                    placeholder='Comments'
                  ></textarea>
                </div>
                <div>
                  <button className='button border-0'>Submit</button>
                </div>
              </form>
            </div>
            <div>
              <h3 className='contact-title mb-4'>Get in Touch with us</h3>
              <div>
                <ul className='ps-0'>
                  <li className='mb-3 d-flex align-items-center gap-15'>
                    <FaHome className='fs-5' />
                    <address className='mb-0'>3a1, Ngo 6, Ao Sen, Ha Dong, Ha Noi</address>
                  </li>
                  <li className='mb-3 d-flex align-items-center gap-15'>
                    <IoCall className='fs-5' />
                    <Link to='tel: +84 38864256'>0388864256</Link>
                  </li>
                  <li className='mb-3 d-flex align-items-center gap-15'>
                    <IoMail className='fs-5' />
                    <Link to='mailto: manhtuan1122001@gmail.com'>manhtuan1122001@gmail.com</Link>
                  </li>
                  <li className='mb-3 d-flex align-items-center gap-15'>
                    <FaClock className='fs-5' />
                    <p className="mb-0">
                      Monday - Friday 10AM - 8PM
                    </p>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </SectionWrappep>
    </>
  )
}

export default Contact
