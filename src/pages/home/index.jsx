import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { LiaShippingFastSolid } from 'react-icons/lia'
import { GoGift } from 'react-icons/go'
import { FaHeadset } from 'react-icons/fa6'
import { TbDiscount2 } from 'react-icons/tb'
import { MdPayments } from 'react-icons/md'
import './style.scss'
import SectionWrappep from '../../components/section_wrapper'
import CategoryItem from '../../components/category_item'
import Marquee from 'react-fast-marquee'
import BlogCard from '../../components/blog_card'
import ProductCard from '../../components/product_card'
import SpecialProductCard from '../../components/special_product_card'
import FamousCard from '../../components/famous_card'
import Meta from '../../components/meta'
import { service } from '../../utils/Data'
import { useDispatch, useSelector } from 'react-redux'
import {
  getListBrandMethod,
  getListCategoryMethod,
  getListCollectionMethod,
  getListProductMethod
} from '../../features/product/middleware'
import Loading from '../loading'
import { getListRecomendProductMethod } from '../../features/review/middleware'
import { getListCartMethod } from '../../features/cart/middleware'

function Home() {
  const generateIcon = (str) => {
    if (str === 'LiaShippingFastSolid') return <LiaShippingFastSolid className='fs-1' />
    else if (str === 'GoGift') return <GoGift className='fs-1' />
    else if (str === 'FaHeadset') return <FaHeadset className='fs-1' />
    else if (str === 'TbDiscount2') return <TbDiscount2 className='fs-1' />
    else if (str === 'MdPayments') return <MdPayments className='fs-1' />
  }
  const dispatch = useDispatch()
  const { list_product, list_category, list_colection, list_brand } = useSelector((state) => state.product)
  const { isLoading } = useSelector((state) => state.product.status)
  const { recommendProductList } = useSelector((state) => state.review)
  const { username, isLogin } = useSelector((state) => state.user)
  const [first_list_collection, setFirst_list_collection] = useState(null) // get element product has colection = 1
  const [bestSaller, setBestSaller] = useState(null) // get best-selling product
  const [flashSale, setFlashSale] = useState(null) // get flash sale product
  useEffect(() => {
    if (!list_product) {
      dispatch(getListProductMethod())
    }
    dispatch(getListCategoryMethod())
    dispatch(getListCollectionMethod())
    dispatch(getListBrandMethod())
  }, [])
  useEffect(() => {
    if (isLogin === 'success') {
      dispatch(getListRecomendProductMethod(username))
      dispatch(getListCartMethod(username))
    }
  }, [isLogin])

  useEffect(() => {
    if (list_product) {
      // get product in first collection
      const filteredArray = list_product.filter((item) => item.colection === 1)
      setFirst_list_collection(filteredArray)

      // get bestseller
      const sortedArray = [...list_product].sort((a, b) => b.quantity_sold - a.quantity_sold)
      setBestSaller(sortedArray.slice(0, 4))

      // get flash sale
      const filteredArrayByDiscount = list_product.filter((item) => item.discount !== null && item.discount.status != 0)
      const sortedArrayByDiscount = [...filteredArrayByDiscount].sort(
        (a, b) => b.discount.discount_percent - a.discount.discount_percent
      )
      setFlashSale(sortedArrayByDiscount.slice(0, 4))
    }
  }, [list_product])

  return (
    <>
      <Meta title='Ecomerce Website' />
      <SectionWrappep className='home-wrapper-1'>
        {isLoading && <Loading />}
        <div className='col-6'>
          <div className='main-banner position-relative'>
            <img
              src='https://cdn.thewirecutter.com/wp-content/media/2023/07/bluetoothheadphones-2048px-0876.jpg'
              alt='main-banner'
              className='img-fluid rounded-3'
            />
            <div className='main-banner-content position-absolute'>
              <h4>Lorem ipsum dolor sit amet.</h4>
              <h5>Lorem ipsum dolor sit</h5>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt repellendus reprehenderit itaque alias
                laudantium assumenda?
              </p>
              <Link to='/' className='button'>
                BUY NOW
              </Link>
            </div>
          </div>
        </div>
        <div className='col-6'>
          <div className='small-banner-list'>
            <div className='small-banner position-relative'>
              <div className='small-banner-img'>
                <img
                  src='https://didongviet.vn/dchannel/wp-content/uploads/2023/09/3-iphone-15-pro-max-bao-nhieu-gb-didongviet-2.jpg'
                  alt='main-banner'
                  className='rounded-3'
                />
              </div>
              <div className='small-banner-content position-absolute'>
                <h4>Lorem ipsum dolor sit amet.</h4>
                <h5>Lorem ipsum dolor sit</h5>
                <p>Lorem ipsum dolor</p>
              </div>
            </div>
            <div className='small-banner position-relative'>
              <div className='small-banner-img'>
                <img
                  src='https://didongviet.vn/dchannel/wp-content/uploads/2023/09/3-iphone-15-pro-max-bao-nhieu-gb-didongviet-2.jpg'
                  alt='main-banner'
                  className='rounded-3'
                />
              </div>
              <div className='small-banner-content position-absolute'>
                <h4>Lorem ipsum dolor sit amet.</h4>
                <h5>Lorem ipsum dolor sit</h5>
                <p>Lorem ipsum dolor</p>
              </div>
            </div>
            <div className='small-banner position-relative'>
              <div className='small-banner-img'>
                <img
                  src='https://didongviet.vn/dchannel/wp-content/uploads/2023/09/3-iphone-15-pro-max-bao-nhieu-gb-didongviet-2.jpg'
                  alt='main-banner'
                  className='rounded-3'
                />
              </div>
              <div className='small-banner-content position-absolute'>
                <h4>Lorem ipsum dolor sit amet.</h4>
                <h5>Lorem ipsum dolor sit</h5>
                <p>Lorem ipsum dolor</p>
              </div>
            </div>
            <div className='small-banner position-relative'>
              <div className='small-banner-img'>
                <img
                  src='https://didongviet.vn/dchannel/wp-content/uploads/2023/09/3-iphone-15-pro-max-bao-nhieu-gb-didongviet-2.jpg'
                  alt='main-banner'
                  className='rounded-3'
                />
              </div>
              <div className='small-banner-content position-absolute'>
                <h4>Lorem ipsum dolor sit amet.</h4>
                <h5>Lorem ipsum dolor sit</h5>
                <p>Lorem ipsum dolor</p>
              </div>
            </div>
          </div>
        </div>
      </SectionWrappep>
      <SectionWrappep className='home-wrapper-2'>
        <div className='col-12'>
          <div className='servies d-flex align-items-center justify-content-between'>
            {service?.map((item, index) => (
              <div className='d-flex align-items-center gap-15' key={index}>
                {generateIcon(item.icon)}
                <div>
                  <h6 className='mb-0'>{item.title}</h6>
                  <p className='mb-0'>{item.tagline}</p>
                </div>
              </div>
            ))}
          </div>
        </div>
      </SectionWrappep>
      <SectionWrappep className='home-wrapper-2'>
        <div className='col-12'>
          <div className='categories d-flex justify-content-between align-items-center'>
            {list_category && list_category.map((item, index) => <CategoryItem key={index} data={item} />)}
          </div>
        </div>
      </SectionWrappep>
      {isLogin === 'success' && (
        <SectionWrappep className='featured-wrapper home-wrapper-2'>
          <div className='col-12'>
            <h3 className='section-heading'>Suggestion for you</h3>
          </div>
          {recommendProductList &&
            recommendProductList.slice(0, 4).map((item, index) => <ProductCard data={item} key={index} />)}
        </SectionWrappep>
      )}
      <SectionWrappep className='featured-wrapper home-wrapper-2'>
        <div className='col-12'>
          <h3 className='section-heading'>Outstanding collection</h3>
        </div>
        <div className='col-12 collection_name'>{list_colection && list_colection[0]?.name}</div>
        {first_list_collection && first_list_collection.map((item, index) => <ProductCard data={item} key={index} />)}
      </SectionWrappep>
      <SectionWrappep className='famous-wrapper home-wrapper-2'>
        <div className='col-12'>
          <h3 className='section-heading'>Best - selling product</h3>
        </div>
        {bestSaller &&
          bestSaller.map((item, index) => <FamousCard data={item} key={index} list_category={list_category} />)}
      </SectionWrappep>
      <SectionWrappep className='special-wrapper home-wrapper-2'>
        <div className='col-12'>
          <h3 className='section-heading'>Flash Sale</h3>
        </div>
        {flashSale &&
          flashSale.map((item, index) => <SpecialProductCard data={item} key={index} list_category={list_category} />)}
      </SectionWrappep>
      <SectionWrappep className='marque-wrapper'>
        <div className='col-12'>
          <div className='marquee-inner-wrapper card-wrapper'>
            <Marquee className='d-flex'>
              {list_brand &&
                list_brand?.map((item, index) => (
                  <div key={index} className='brand-logo mx-4 w-100'>
                    <img src={item?.thumbnail} alt='brand' />
                  </div>
                ))}
            </Marquee>
          </div>
        </div>
      </SectionWrappep>
      <SectionWrappep className='blog-wrapper home-wrapper-2'>
        <div className='col-12'>
          <h3 className='section-heading'>Our Latest Blogs</h3>
        </div>
        <div className='col-3'>
          <BlogCard />
        </div>
        <div className='col-3'>
          <BlogCard />
        </div>
        <div className='col-3'>
          <BlogCard />
        </div>
        <div className='col-3'>
          <BlogCard />
        </div>
      </SectionWrappep>
    </>
  )
}

export default Home
