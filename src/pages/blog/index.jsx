import React from 'react'
import Meta from '../../components/meta'
import BreadCrumb from '../../components/bread_crumb'
import SectionWrappep from '../../components/section_wrapper'
import BlogCard from '../../components/blog_card'

function Blog() {
  return (
    <>
      <Meta title='Blogs' />
      <BreadCrumb title='Blogs' />
      <SectionWrappep className='blog-wrapper home-wrapper-2'>
        <div className='col-3'>
          <div className='filter-card mb-3'>
            <h3 className='filter-title'>Find By Categories</h3>
            <div>
              <ul>
                <li>Watch</li>
                <li>TV</li>
                <li>Camera</li>
                <li>Laptop</li>
              </ul>
            </div>
          </div>
        </div>
        <div className='col-9'>
          <div className='row'>
            <div className='col-6 mb-3'>
              <BlogCard />
            </div>
            <div className='col-6 mb-3'>
              <BlogCard />
            </div>
            <div className='col-6 mb-3'>
              <BlogCard />
            </div>
            <div className='col-6 mb-3'>
              <BlogCard />
            </div>
          </div>
        </div>
      </SectionWrappep>
    </>
  )
}

export default Blog
