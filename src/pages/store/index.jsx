import React, { useEffect, useState } from 'react'
import BreadCrumb from '../../components/bread_crumb'
import Meta from '../../components/meta'
import SectionWrappep from '../../components/section_wrapper'
import './style.scss'
import ReactStars from 'react-rating-stars-component'
import { FiGrid } from 'react-icons/fi'
import { CiBoxList } from 'react-icons/ci'
import ProductCard from '../../components/product_card'
import { useDispatch, useSelector } from 'react-redux'
import { getListProductMethod } from '../../features/product/middleware'
import Loading from '../loading'

function Store() {
  const [grid, setGrid] = useState(3)
  const { list_product, list_category, list_brand } = useSelector((state) => state.product)
  const { isLoading } = useSelector((state) => state.product.status)
  const [list, setList] = useState([])
  const [brandCondition, setBrandCondition] = useState([])
  const [statusCondition, setStatusCondition] = useState([])
  const [priceCondition, setPriceCondition] = useState(['', ''])
  const [productOnStock, setProductOnStock] = useState(0)
  const [categoryId, setCategoryId] = useState(0)
  const [sortedCondition, setSortedCondition] = useState(0)
  const dispatch = useDispatch()
  useEffect(() => {
    if (!list_product) {
      dispatch(getListProductMethod())
    }
    setList(list_product)
  }, [])

  useEffect(() => {
    let count = 0
    if (list) {
      count = list.filter((product) => product.status === 'On Stock').length
    } else {
      count = list_product.filter((product) => product.status === 'On Stock').length
    }
    setProductOnStock(count)
  }, [list])

  const handleBrandCheckboxChange = (id) => {
    if (brandCondition.includes(id)) {
      brandCondition.splice(brandCondition.indexOf(id), 1)
    } else {
      brandCondition.push(id)
    }
    setBrandCondition([...brandCondition])
  }

  const handleStockCheckboxChange = (value) => {
    if (statusCondition.includes(value)) {
      statusCondition.splice(statusCondition.indexOf(value), 1)
    } else {
      statusCondition.push(value)
    }
    setStatusCondition([...statusCondition])
  }

  const handleFilterByCategory = (id) => {
    if (id === 0) {
      setCategoryId(id)
      setList(list_product)
    } else {
      let list_product_filter = [...list]
      list_product_filter = list_product.filter((item) => item.category == id)
      setList(list_product_filter)
      setCategoryId(id)
    }
    setBrandCondition([])
    setStatusCondition([])
    setPriceCondition(['', ''])
  }

  const handleFilter = () => {
    let list_product = [...list]
    // filter by brand
    if (brandCondition.length > 0) {
      list_product = list_product.filter((item) => {
        for (const value of brandCondition) {
          if (item.brand === value) return item
        }
      })
    }

    // filter by stock
    if (statusCondition.length > 0) {
      list_product = list_product.filter((item) => {
        for (const value of statusCondition) {
          if (item.status === value) return item
        }
      })
    }

    // filter by stock
    if (priceCondition[0]) {
      list_product = list_product.filter((item) => {
        if (item.price >= priceCondition[0]) return item
      })
    }
    if (priceCondition[1]) {
      list_product = list_product.filter((item) => {
        if (item.price <= priceCondition[1]) return item
      })
    }
    setList(list_product)
  }

  const handleSorted = (condition) => {
    setSortedCondition(condition)
    let new_list = [...list]
    if (condition === '0') {
      return
    }
    if (condition === '1') {
      new_list.sort((a, b) => b.quantity_sold - a.quantity_sold)
      setList(new_list)
      return
    }
    if (condition === '2') {
      new_list.sort((a, b) => a.title.localeCompare(b.title))
      setList(new_list)
      return
    }
    if (condition === '3') {
      new_list.sort((a, b) => b.title.localeCompare(a.title))
      setList(new_list)
      return
    }
    if (condition === '4') {
      new_list.sort((a, b) => a.price - b.price)
      setList(new_list)
      return
    }
    if (condition === '5') {
      new_list.sort((a, b) => b.price - a.price)
      setList(new_list)
      return
    }
    if (condition === '6') {
      new_list.sort((a, b) => new Date(a.created_at) - new Date(b.created_at))
      setList(new_list)
      return
    }
    if (condition === '7') {
      new_list.sort((a, b) => new Date(b.created_at) - new Date(a.created_at))
      setList(new_list)
      return
    }
  }
  return (
    <>
      <Meta title='Our Store' />
      <BreadCrumb title='Our Store' />
      {isLoading && <Loading />}
      <SectionWrappep className='store-wrapper home-wrapper-2'>
        <div className='col-3'>
          <div className='filter-card mb-3'>
            <h3 className='filter-title'>Shop By Categories</h3>
            <div>
              <ul>
                <li className={categoryId == 0 ? 'category-active' : ''} onClick={() => handleFilterByCategory(0)}>
                  All
                </li>
                {list_category &&
                  list_category.map((item, index) => (
                    <li
                      key={index}
                      onClick={() => handleFilterByCategory(item.id)}
                      className={categoryId == item.id ? 'category-active' : ''}
                    >
                      {item.name}
                    </li>
                  ))}
              </ul>
            </div>
          </div>
          <div className='filter-card mb-3'>
            <h3 className='filter-title'>Filter By</h3>

            <div>
              <h5 className='sub-title'>Brand</h5>
              <div>
                {list_brand &&
                  list_brand.map((item, index) => (
                    <div className='form-check' key={index}>
                      <input
                        className='form-check-input'
                        type='checkbox'
                        checked={brandCondition.includes(item.id)}
                        id={`form-check-input-${index}`}
                        onChange={() => handleBrandCheckboxChange(item.id)}
                      />
                      <label className='form-check-label' htmlFor={`form-check-input-${index}`}>
                        {item.name}
                      </label>
                    </div>
                  ))}
              </div>
              <h5 className='sub-title'>Availablity</h5>
              <div>
                <div className='form-check'>
                  <input
                    className='form-check-input'
                    type='checkbox'
                    value=''
                    id='form-check-input-stock-1'
                    checked={statusCondition.includes('On Stock')}
                    onChange={() => handleStockCheckboxChange('On Stock')}
                  />
                  <label className='form-check-label' htmlFor='form-check-input-stock-1'>
                    On Stock ({productOnStock})
                  </label>
                </div>
                <div className='form-check'>
                  <input
                    type='checkbox'
                    className='form-check-input'
                    value=''
                    id='form-check-input-stock-2'
                    checked={statusCondition.includes('Out of Stock')}
                    onChange={() => handleStockCheckboxChange('Out of Stock')}
                  />
                  <label className='form-check-label' htmlFor='form-check-input-stock-2'>
                    Out of Stock ({list && list.length - productOnStock})
                  </label>
                </div>
              </div>

              <h5 className='sub-title'>Price (VND)</h5>
              <div className='d-flex align-items-center gap-10'>
                <div className='form-floating'>
                  <input
                    type='number'
                    className='form-control'
                    name='formId1'
                    id='formId1'
                    placeholder=''
                    value={priceCondition[0]}
                    onChange={(e) =>
                      setPriceCondition((prev) => {
                        prev[0] = e.target.value
                        return [...prev]
                      })
                    }
                  />
                  <label htmlFor='formId1'>From</label>
                </div>
                <div className='form-floating'>
                  <input
                    type='number'
                    className='form-control'
                    name='formId2'
                    id='formId2'
                    placeholder=''
                    value={priceCondition[1]}
                    onChange={(e) =>
                      setPriceCondition((prev) => {
                        prev[1] = e.target.value
                        return [...prev]
                      })
                    }
                  />
                  <label htmlFor='formId2'>To</label>
                </div>
              </div>
            </div>
            <div className='text-center mt-4'>
              <button className='button' onClick={handleFilter}>
                Product Filtering
              </button>
            </div>
          </div>
          <div className='filter-card mb-3'>
            <h3 className='filter-title'>Random Product</h3>
            <div>
              <div className='random-products py-3 d-flex'>
                <div className='w-50'>
                  <img
                    src='https://cdn.tgdd.vn/Products/Images/7077/289804/apple-watch-s8-41mm-trang-kem-tn-600x600.jpg'
                    alt='watch'
                    className='img-fluid'
                  />
                </div>
                <div className='w-50'>
                  <h5>Lorem ipsum dolor sit amet consectetur.</h5>
                  <ReactStars count={5} size={24} value={4} edit={false} activeColor='#ffd700' />
                  <b>12.000.000 VND</b>
                </div>
              </div>
              <div className='random-products py-3 d-flex'>
                <div className='w-50'>
                  <img
                    src='https://cdn.tgdd.vn/Products/Images/7077/289804/apple-watch-s8-41mm-trang-kem-tn-600x600.jpg'
                    alt='watch'
                    className='img-fluid'
                  />
                </div>
                <div className='w-50'>
                  <h5>Lorem ipsum dolor sit amet consectetur.</h5>
                  <ReactStars count={5} size={24} value={4} edit={false} activeColor='#ffd700' />
                  <b>12.000.000 VND</b>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-9'>
          {/* filter bar */}
          <div className='filter-sort-grid mb-4'>
            <div className='d-flex justify-content-between align-items-center'>
              <div className='d-flex align-items-center gap-10'>
                <p className='mb-0 d-block' style={{ width: '100px' }}>
                  Sort By:
                </p>
                <select
                  name=''
                  className='form-control form-select'
                  value={sortedCondition}
                  onChange={(e) => handleSorted(e.target.value)}
                >
                  <option value={0}>Featured</option>
                  <option value={1}>Best selling</option>
                  <option value={2}>A-Z</option>
                  <option value={3}>Z-A</option>
                  <option value={4}>Price, low to high</option>
                  <option value={5}>Price, high to low</option>
                  <option value={6}>Date, old to new</option>
                  <option value={7}>Date, new to old</option>
                </select>
              </div>
              <div className='d-flex align-items-center gap-10'>
                <p className='total-products mb-0'>{list.length} Products</p>
                <div className='d-flex gap-10 align-items-center grid'>
                  <FiGrid className={`fs-5 d-block ${grid === 3 ? 'active' : ''}`} onClick={() => setGrid(3)} />
                  <CiBoxList className={`fs-5 d-block ${grid === 12 ? 'active' : ''}`} onClick={() => setGrid(12)} />
                </div>
              </div>
            </div>
          </div>
          {/* product-list */}
          <div className='product-list pb-5'>
            {list.length === 0 ? (
              <div className='text-center' style={{ fontSize: '1.2rem' }}>
                Product not found
              </div>
            ) : (
              <div className='d-flex flex-wrap gap-10'>
                {list && list.map((item, index) => <ProductCard data={item} key={index} grid={grid} />)}
              </div>
            )}
          </div>
        </div>
      </SectionWrappep>
    </>
  )
}

export default Store
