import React from 'react'
import Meta from '../../components/meta'
import BreadCrumb from '../../components/bread_crumb'
import PolicyItem from '../../components/policy_item'
import SectionWrappep from '../../components/section_wrapper'
import './style.scss'

function PrivacyPolicy() {
  return (
    <>
      <Meta title='Privacy Policy' />
      <BreadCrumb title='Privacy Policy' />
      <SectionWrappep className='policy-wrapper home-wrapper-2'>
        <div className="col-12">
          <div className="policy">
            <h3 className="title text-center mb-5">Privacy Policy</h3>
            <PolicyItem />
            <PolicyItem />
            <PolicyItem />
            <PolicyItem />
          </div>
        </div>
      </SectionWrappep>
    </>
  )
}

export default PrivacyPolicy
