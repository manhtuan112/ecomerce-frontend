import React from 'react'
import Meta from '../../components/meta'
import BreadCrumb from '../../components/bread_crumb'
import SectionWrappep from '../../components/section_wrapper'
import PolicyItem from '../../components/policy_item'
import './style.scss'

function RefundPolicy() {
  return (
    <>
      <Meta title='Refund Policy' />
      <BreadCrumb title='Refund Policy' />
      <SectionWrappep className='policy-wrapper home-wrapper-2'>
        <div className="col-12">
          <div className="policy">
            <h3 className="title text-center mb-5">Refund Policy</h3>
            <PolicyItem />
            <PolicyItem />
            <PolicyItem />
            <PolicyItem />
          </div>
        </div>
      </SectionWrappep>
    </>
  )
}

export default RefundPolicy