import React from 'react'
import Meta from '../../components/meta'
import BreadCrumb from '../../components/bread_crumb'
import SectionWrappep from '../../components/section_wrapper'
import './style.scss'
import PolicyItem from '../../components/policy_item'
function TermAndConditions() {
  return (
    <>
      <Meta title='Term And Conditions' />
      <BreadCrumb title='Term And Conditions' />
      <SectionWrappep className='policy-wrapper home-wrapper-2'>
        <div className="col-12">
          <div className="policy">
            <h3 className="title text-center mb-5">Term And Conditions</h3>
            <PolicyItem />
            <PolicyItem />
            <PolicyItem />
            <PolicyItem />
          </div>
        </div>
      </SectionWrappep>
    </>
  )
}

export default TermAndConditions