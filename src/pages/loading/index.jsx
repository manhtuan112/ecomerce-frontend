import React from 'react'
import './style.scss'

function Loading() {
  return (
    <div className='loading-wrapper'>
      <div className='rocket-loader'>
        <div className='rocket'>
          <div className='rocket-extras'></div>
          <div className='jet'>
            <span></span>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Loading
