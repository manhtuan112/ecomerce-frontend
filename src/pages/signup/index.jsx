import React, { useState, useEffect } from 'react'
import Meta from '../../components/meta'
import BreadCrumb from '../../components/bread_crumb'
import SectionWrappep from '../../components/section_wrapper'
import { Link, useNavigate } from 'react-router-dom'
import { createAxiosInstance } from '../../utils/axiosInstance'
import Swal from 'sweetalert2'
import { signupMethod } from '../../features/authenticate/middleware'
import { useDispatch, useSelector } from 'react-redux'
import Loading from '../loading'
import { setSignup } from '../../features/authenticate/userSlice'

function Signup() {
  const nav = useNavigate()
  const dispatch = useDispatch()
  const [email, setEmail] = useState('')
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [rePassword, setRePassword] = useState('')
  const [firstname, setFirstname] = useState('')
  const [lastname, setLastname] = useState('')
  const { isSignup, failedMessage } = useSelector((state) => state.user)

  useEffect(() => {
    if (isSignup === 'success') {
      Swal.fire({
        icon: 'success',
        title: 'Signup Success!!!',
        confirmButtonText: 'Oke'
      }).then((result) => {
        if (result.isConfirmed) {
          nav('/login')
        }
      })
    } else if (isSignup === 'failed') {
      Swal.fire({
        icon: 'error',
        title: 'Signup Failed!!!',
        text: failedMessage
      })
      dispatch(setSignup({isSignup: 'false'}))
    }
  }, [isSignup])

  const handleSignup = (e) => {
    e.preventDefault()
    if (password !== rePassword) {
      Swal.fire({
        icon: 'error',
        title: 'Signup Failed',
        text: 'Password and Re Password not same!!!'
      })
    } else {
      const data = { username, password, email, first_name: firstname, last_name: lastname }
      dispatch(signupMethod(data))
    }
  }
  return (
    <>
      <Meta title='Sign Up' />
      <BreadCrumb title='Sign Up' />
      {isSignup === 'pending' && <Loading />}
      <SectionWrappep className='signup-wrapper home-wrapper-2'>
        <div className='col-12'>
          <div className='auth-card'>
            <h3 className='text-center mb-3'>Create Account</h3>
            <form onSubmit={handleSignup} className='d-flex flex-column gap-15'>
              <div>
                <input
                  type='text'
                  name='firstname'
                  placeholder='First Name'
                  value={firstname}
                  onChange={(e) => setFirstname(e.target.value)}
                  className='form-control'
                />
              </div>
              <div>
                <input
                  type='text'
                  name='lastname'
                  placeholder='Last Name'
                  value={lastname}
                  onChange={(e) => setLastname(e.target.value)}
                  className='form-control'
                />
              </div>
              <div>
                <input
                  type='text'
                  name='username'
                  placeholder='Username'
                  value={username}
                  onChange={(e) => setUsername(e.target.value)}
                  className='form-control'
                />
              </div>
              <div>
                <input
                  type='email'
                  name='email'
                  placeholder='Email'
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  className='form-control'
                />
              </div>
              <div>
                <input
                  type='password'
                  name='password'
                  placeholder='Password'
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  className='form-control'
                />
              </div>
              <div>
                <input
                  type='password'
                  name='re-password'
                  placeholder='Re Password'
                  value={rePassword}
                  onChange={(e) => setRePassword(e.target.value)}
                  className='form-control'
                />
              </div>
              <div>
                <div className='d-flex justify-content-center gap-10 mt-3 flex-column align-items-center'>
                  <button className='button border-0'>Sign Up</button>
                  <Link to='/login'>Cancel</Link>
                </div>
              </div>
            </form>
          </div>
        </div>
      </SectionWrappep>
    </>
  )
}

export default Signup
