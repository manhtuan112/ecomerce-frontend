import React, { useState } from 'react'
import Meta from '../../components/meta'
import BreadCrumb from '../../components/bread_crumb'
import SectionWrappep from '../../components/section_wrapper'
import { ImProfile } from 'react-icons/im'
import { MdOutlinePayment, MdKey } from 'react-icons/md'
import { IoIosLogOut } from 'react-icons/io'
import './style.scss'
import PersonalDetails from '../../components/personal_details_card'
import ChangePasswordCard from '../../components/change_password_card'
import Swal from 'sweetalert2'
import { useDispatch, useSelector } from 'react-redux'
import { resetState } from '../../features/authenticate/userSlice'
import { removeLocalStorage } from '../../utils/localStorage'
import { useNavigate } from 'react-router-dom'
import OrderHistoryCard from '../../components/order_history_card'
function Profile() {
  const [currentSection, setCurrentSection] = useState(1)
  const dispatch = useDispatch()
  const nav = useNavigate()
  const handleLogout = () => {
    Swal.fire({
      icon: 'question',
      title: 'Do you want logout?',
      showDenyButton: true,
      confirmButtonText: 'Yes',
      denyButtonText: `No`
    }).then((result) => {
      if (result.isConfirmed) {
        removeLocalStorage('accessToken')
        dispatch(resetState())
        nav('/')
      } else if (result.isDenied) {
      }
    })
  }
  return (
    <>
      <Meta title='User Information' />
      <BreadCrumb title='User Information' />
      <SectionWrappep className='user-info-wrapper home-wrapper-2'>
        <div className='col-3'>
          <div className='list-group box-shadown' id='list-tab' role='tablist'>
            <button
              className={`list-group-item list-group-item-action ${
                currentSection === 1 && 'active'
              } d-flex align-items-center gap-30`}
              id='list-home-list'
              data-toggle='list'
              role='tab'
              aria-controls='home'
              onClick={() => setCurrentSection(1)}
            >
              <ImProfile className='fs-3' />
              Personal Details
            </button>
            <button
              className={`list-group-item list-group-item-action ${
                currentSection === 2 && 'active'
              } d-flex align-items-center gap-30`}
              id='list-home-list'
              data-toggle='list'
              role='tab'
              aria-controls='home'
              onClick={() => setCurrentSection(2)}
            >
              <MdOutlinePayment className='fs-3' />
              Payment History
            </button>
            <button
              className={`list-group-item list-group-item-action ${
                currentSection === 3 && 'active'
              } d-flex align-items-center gap-30`}
              id='list-home-list'
              data-toggle='list'
              role='tab'
              aria-controls='home'
              onClick={() => setCurrentSection(3)}
            >
              <MdKey className='fs-3' />
              Change Password
            </button>
            <button
              className='list-group-item list-group-item-action d-flex align-items-center gap-30'
              id='list-settings-list'
              data-toggle='list'
              role='tab'
              aria-controls='settings'
              onClick={handleLogout}
            >
              <IoIosLogOut className='fs-3' color='red' />
              Logout
            </button>
          </div>
        </div>
        <div className='col-9'>
          <div className='row'>
            <div className='col-12'>
              {currentSection === 1 && <PersonalDetails />}
              {currentSection === 2 && <OrderHistoryCard />}
              {currentSection === 3 && <ChangePasswordCard />}
            </div>
          </div>
        </div>
      </SectionWrappep>
    </>
  )
}

export default Profile
