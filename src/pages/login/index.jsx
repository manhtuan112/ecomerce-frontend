import React, { useState, useEffect } from 'react'
import Meta from '../../components/meta'
import BreadCrumb from '../../components/bread_crumb'
import SectionWrappep from '../../components/section_wrapper'
import { Link } from 'react-router-dom'
import './style.scss'
import Swal from 'sweetalert2'
import { useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { loginMethod } from '../../features/authenticate/middleware'
import Loading from '../loading'
import { unwrapResult } from '@reduxjs/toolkit'
import { useFormik } from 'formik'
import * as yup from 'yup'
function Login() {
  const nav = useNavigate()
  const dispatch = useDispatch()
  let schema = yup.object().shape({
    username: yup.string().trim().required('Username is Required'),
    password: yup.string().trim().required('Password is Required')
  })
  const { isLogin, failedMessage } = useSelector((state) => state.user)

  const formik = useFormik({
    initialValues: {
      username: '',
      password: ''
    },
    validationSchema: schema,
    onSubmit: (values) => {
      handleLogin(values)
    }
  })

  const handleLogin = async (values) => {
    try {
      let data = { username: values.username, password: values.password }
      const resultAction = await dispatch(loginMethod(data))
      const user = unwrapResult(resultAction)
      Swal.fire({
        icon: 'success',
        title: `Login Success!!!`,
        confirmButtonText: 'Oke'
      }).then((result) => {
        if (result.isConfirmed) {
          nav('/')
        }
      })
    } catch (e) {
      Swal.fire({
        icon: 'error',
        title: 'Login Failed!!!',
        text: e
      })
    }
  }
  return (
    <>
      <Meta title='Login' />
      <BreadCrumb title='Login' />
      {isLogin === 'pending' && <Loading />}
      <SectionWrappep className='login-wrapper home-wrapper-2'>
        <div className='col-12'>
          <div className='auth-card'>
            <h3 className='text-center mb-3'>Login</h3>
            <form onSubmit={formik.handleSubmit} className='d-flex flex-column gap-15'>
              <div>
                <input
                  type='text'
                  name='username'
                  placeholder='Username'
                  className='form-control'
                  onChange={formik.handleChange('username')}
                  value={formik.values.username}
                />
              </div>
              <div className='error mt-1'>{formik.touched.username && formik.errors.username}</div>
              <div className='mt-1'>
                <input
                  type='password'
                  name='password'
                  placeholder='Password'
                  className='form-control'
                  onChange={formik.handleChange('password')}
                  value={formik.values.password}
                />
              </div>
              <div className='error mt-1'>{formik.touched.password && formik.errors.password}</div>
              <div>
                <div className='text-end'>
                  <Link to='/forget-password' className='forgot-password'>
                    Forgot Password?
                  </Link>
                </div>

                <div className='d-flex justify-content-center gap-10 mt-3'>
                  <button className='button border-0' type='submit'>Login</button>
                  <Link to='/signup' className='button signup'>
                    Sign Up
                  </Link>
                </div>
              </div>
            </form>
          </div>
        </div>
      </SectionWrappep>
    </>
  )
}

export default Login
