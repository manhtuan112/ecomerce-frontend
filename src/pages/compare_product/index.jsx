import React from 'react'
import Meta from '../../components/meta'
import BreadCrumb from '../../components/bread_crumb'
import SectionWrappep from '../../components/section_wrapper'
import CompareProductCard from '../../components/compare_product_card'
function CompareProduct() {
  return (
    <>
      <Meta title='Compare Product' />
      <BreadCrumb title='Compare Product' />
      <SectionWrappep className='compare-product-wrapper home-wrapper-2'>
        <div className='col-3'>
          <CompareProductCard />
        </div>
        <div className='col-3'>
          <CompareProductCard />
        </div>
        <div className='col-3'>
          <CompareProductCard />
        </div>
        <div className='col-3'>
          <CompareProductCard />
        </div>
      </SectionWrappep>
    </>
  )
}

export default CompareProduct
