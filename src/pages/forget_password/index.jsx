import React, { useEffect, useState } from 'react'
import Meta from '../../components/meta'
import BreadCrumb from '../../components/bread_crumb'
import SectionWrappep from '../../components/section_wrapper'
import { Link, useNavigate } from 'react-router-dom'
import Loading from '../loading'
import { useDispatch, useSelector } from 'react-redux'
import { changePasswordWithOtpMethod, forgotPassword, verifyOtp } from '../../features/authenticate/middleware'
import CustomToastContainer from '../../components/CustomToastContainer'
import { toast, ToastContainer } from 'react-toastify'
import { unwrapResult } from '@reduxjs/toolkit'
import { useFormik } from 'formik'
import Swal from 'sweetalert2'

import * as yup from 'yup'
function ForgetPassword() {
  const [email, setEmail] = useState('')
  const [page, setPage] = useState(1)
  const [otp, setOtp] = useState('')
  const dispatch = useDispatch()
  const nav = useNavigate()
  let schema = yup.object().shape({
    password: yup.string().trim().required('Password is Required'),
    re_password: yup
      .string()
      .trim()
      .oneOf([yup.ref('password'), null], 'Re-password must be same password')
      .required('Re Password is Required')
  })

  const formik = useFormik({
    initialValues: {
      password: '',
      re_password: ''
    },
    validationSchema: schema,
    onSubmit: (values) => {
      handleResetPassword(values)
    }
  })
  const status = useSelector((state) => state.user.status)
  const handleSendEmail = async (e) => {
    e.preventDefault()
    try {
      const data = { email }
      const resultAction = await dispatch(forgotPassword(data))
      const result = unwrapResult(resultAction)
      toast.success(
        <CustomToastContainer header='Success' message='Sent email successfull. Please check your email to get otp' />,
        {
          position: 'top-right',
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: 'light'
        }
      )
      setPage(2)
    } catch (e) {
      toast.error(<CustomToastContainer header='Error' message={e} />, {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light'
      })
    }
  }

  const handleVerifyOTP = async (e) => {
    e.preventDefault()
    try {
      const data = { otp }
      const resultAction = await dispatch(verifyOtp(data))
      const result = unwrapResult(resultAction)
      toast.success(
        <CustomToastContainer header='Success' message='OTP is exactly. Please enter your new password' />,
        {
          position: 'top-right',
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: 'light'
        }
      )
      setPage(3)
    } catch (e) {
      toast.error(<CustomToastContainer header='Error' message={e} />, {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light'
      })
    }
  }

  const handleResetPassword = async (values) => {
    try {
      const data = { email, password: values.password }
      const resultAction = await dispatch(changePasswordWithOtpMethod(data))
      const result = unwrapResult(resultAction)
      Swal.fire({
        icon: 'success',
        title: 'Reset Password Success!!!',
        confirmButtonText: 'Oke'
      }).then((result) => {
        if (result.isConfirmed) {
          nav('/login')
        }
      })
    } catch (e) {
      toast.error(<CustomToastContainer header='Error' message={e} />, {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light'
      })
    }
  }

  return (
    <>
      <Meta title='Forget Password' />
      <BreadCrumb title='Forget Password' />
      <ToastContainer />
      {status.isLoading && <Loading />}
      <SectionWrappep className='forget-password-wrapper home-wrapper-2'>
        <div className='col-12'>
          <div className='auth-card'>
            {page === 1 && (
              <>
                <h3 className='text-center mb-3'>Reset Your Password</h3>
                <p className='text-center mt-2 mb-3'>We will send you OTP code via email to reset your password.</p>
                <form onSubmit={handleSendEmail} className='d-flex flex-column gap-15'>
                  <div>
                    <input
                      type='email'
                      name='email'
                      placeholder='Email'
                      className='form-control'
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                    />
                  </div>
                  <div>
                    <div className='d-flex justify-content-center gap-10 mt-3 flex-column align-items-center'>
                      <button className='button border-0'>Submit</button>
                      <Link to='/login'>Cancel</Link>
                    </div>
                  </div>
                </form>
              </>
            )}
            {page === 2 && (
              <>
                <h3 className='text-center mb-3'>Verify OTP</h3>
                <form onSubmit={handleVerifyOTP} className='d-flex flex-column gap-15'>
                  <div>
                    <input
                      type='text'
                      name='otp'
                      placeholder='Input OTP'
                      className='form-control'
                      value={otp}
                      onChange={(e) => setOtp(e.target.value)}
                    />
                  </div>
                  <div>
                    <div className='d-flex justify-content-center gap-10 mt-3 flex-column align-items-center'>
                      <button className='button border-0' type='submit'>Verify</button>
                      <Link to='/login'>Cancel</Link>
                    </div>
                  </div>
                </form>
              </>
            )}
            {page === 3 && (
              <>
                <h3 className='text-center mb-3'>Reset Password</h3>
                <form onSubmit={formik.handleSubmit} className='d-flex flex-column gap-15'>
                  <div>
                    <input
                      type='password'
                      name='password'
                      placeholder='Password'
                      onChange={formik.handleChange('password')}
                      value={formik.values.password}
                      className='form-control'
                    />
                  </div>
                  <div className='error mt-1'>{formik.touched.password && formik.errors.password}</div>

                  <div>
                    <input
                      type='password'
                      name='re-password'
                      placeholder='Re Password'
                      onChange={formik.handleChange('re_password')}
                      value={formik.values.re_password}
                      className='form-control'
                    />
                  </div>
                  <div className='error mt-1'>{formik.touched.re_password && formik.errors.re_password}</div>

                  <div>
                    <div className='d-flex justify-content-center gap-10 mt-3 flex-column align-items-center'>
                      <button className='button border-0' type='submit'>Submit</button>
                      <Link to='/login'>Cancel</Link>
                    </div>
                  </div>
                </form>
              </>
            )}
          </div>
        </div>
      </SectionWrappep>
    </>
  )
}

export default ForgetPassword
