import React, { useEffect } from 'react'
import Meta from '../../components/meta'
import BreadCrumb from '../../components/bread_crumb'
import SectionWrappep from '../../components/section_wrapper'
import './style.scss'
import { Link, useSearchParams } from 'react-router-dom'
import { getLocalStorage } from '../../utils/localStorage'
import { deleteCartMethod } from '../../features/cart/middleware'
import { useDispatch, useSelector } from 'react-redux'
import { setNumberCart } from '../../features/cart/cartSlice'
import { createShipmentMethod } from '../../features/shipment/middleware'
import { unwrapResult } from '@reduxjs/toolkit'
import { updateOrderMethod } from '../../features/order/middleware'
function PaymentSuccess() {
  const [searchParams, setSearchParams] = useSearchParams()
  const dispatch = useDispatch()
  const shipmentInfo = getLocalStorage('shipmentInfo')

  const orderId = searchParams.get('vnp_TxnRef')?.split('_')[0] || searchParams.get('orderId')?.split('_')[0]
  useEffect(() => {
    // xoa cart, update order => de sau
    //Delete and update cart
    let list_delete_cart = getLocalStorage('listCartDelete')
    if (!list_delete_cart) {
      return
    }
    list_delete_cart = list_delete_cart.split(',')
    const array = []
    for (const i in list_delete_cart) {
      array.push(dispatch(deleteCartMethod(list_delete_cart[i])))
    }
    array.push()
    Promise.all(array)
      .then((results) => {
        dispatch(setNumberCart())
      })
      .catch((error) => {
        dispatch(setNumberCart())
      })

    // create shipment
    const createShipmentandUpdateOrder = async () => {
      try {
        const resultAction = await dispatch(createShipmentMethod(shipmentInfo))
        const result = unwrapResult(resultAction)
        const data = {
          id: orderId,
          status: 'payment_success',
          shipment_id: result.data.id,
          paymentType: searchParams.get('vnp_TxnRef') ? 'vnpay' : 'zalopay'
        }
        const resultAction2 = await dispatch(updateOrderMethod(data))
      } catch (e) {
        console.log(e)
      }
    }
    createShipmentandUpdateOrder()
  }, [])

  return (
    <>
      <Meta title='Payment Success' />
      <SectionWrappep classNameName='payment-wrapper'>
        <div className='card'>
          <div className='card-icon'>
            <i className='checkmark'>✓</i>
          </div>
          <h1>Success</h1>
          <p>You have successfully paid for your order #{orderId}</p>
          <p className='mb-4'>Please return to the home page to continue shopping</p>
          <Link to='/' className='button'>
            Return Home Page
          </Link>
        </div>
      </SectionWrappep>
    </>
  )
}

export default PaymentSuccess
