import React, { useEffect, useState } from 'react'
import SectionWrappep from '../../components/section_wrapper'
import { Link } from 'react-router-dom'
import { IoMdArrowBack } from 'react-icons/io'
import './style.scss'
import CheckoutItem from '../../components/checkout_item'
import Meta from '../../components/meta'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import { getOrderInfoMethod, paymentVNPayMethod, paymentZaloPayMethod } from '../../features/order/middleware'
import CustomToastContainer from '../../components/CustomToastContainer'
import { toast, ToastContainer } from 'react-toastify'
import { unwrapResult } from '@reduxjs/toolkit'
import { convertMoneyFormat } from '../../features/common'
import { v4 } from 'uuid'
import Col from 'react-bootstrap/Col'
import ListGroup from 'react-bootstrap/ListGroup'
import Row from 'react-bootstrap/Row'
import Tab from 'react-bootstrap/Tab'
import { getGeneralUserInfoMethod } from '../../features/authenticate/middleware'
import { addShipmentAddressMethod, getShipmentAddressListMethod } from '../../features/shipment/middleware'
import ModalAddAddress from '../../components/modal_address'
import { service } from '../../utils/Data'
import { setShipmentInfo } from '../../features/shipment/shipmentSlice'

function Checkout() {
  const { id } = useParams()
  const [listOrderItem, setlistOrderItem] = useState([])
  const [infoUser, setInfoUser] = useState({})
  const [listAddress, setListAddress] = useState([])
  const [subTotalPrice, setSubTotalPrice] = useState(0)
  const [currentAddress, setCurrentAddress] = useState(0) // Choose default address for shipment
  const [modalShow, setModalShow] = useState(false)
  const [shipfee, setShipfee] = useState(25000)
  const dispath = useDispatch()
  useEffect(() => {
    const fetch = async () => {
      try {
        let resultAction = await dispath(getOrderInfoMethod(id))
        let result = unwrapResult(resultAction)
        const list_data = result.data.order_item_list
        let subTotal = list_data.reduce((acc, curr) => {
          // calculate subTotal price
          return acc + curr.price * curr.quantity
        }, 0)
        setlistOrderItem(result.data.order_item_list)
        // get address
        resultAction = await dispath(getGeneralUserInfoMethod())
        result = unwrapResult(resultAction)
        setInfoUser(result.data)
        let listAddressTmp = []
        listAddressTmp.push({
          defaultAddress: true,
          address: result.data.address?.home_number,
          ward: result.data.address?.street,
          district: result.data.address?.state,
          province: result.data.address?.city
        })

        // get list shipment address
        resultAction = await dispath(getShipmentAddressListMethod(result.data.username))
        result = unwrapResult(resultAction)
        result.data.forEach((item) => {
          listAddressTmp.push({
            address: item.home_number,
            ward: item.commune,
            district: item.state,
            province: item.city,
            id: item.id
          })
        })
        setListAddress(listAddressTmp)
        setSubTotalPrice(subTotal)
        
      } catch (e) {
        toast.error(<CustomToastContainer header='Error' message={e} />, {
          position: 'top-right',
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
          theme: 'light'
        })
      }
    }
    fetch()
  }, [id])

  useEffect(() => {
    if (currentAddress === 0) {
      setShipfee(25000)
      return
    }
    const data = {
      to_name: infoUser.first_name + ' ' + infoUser.last_name,
      to_phone: infoUser.telephoneNumber,
      to_address: listAddress[currentAddress].address,
      to_ward_name: listAddress[currentAddress].ward,
      to_district_name: listAddress[currentAddress].district,
      to_province_name: listAddress[currentAddress].province,
      weight: 7000,
      service_type_id: 5,
      payment_type_id: 2,
      required_note: 'CHOXEMHANGKHONGTHU',
      items: [
        {
          name: 'Tivi',
          quantity: 1,
          weight: 1000
        }
      ]
    }
    fetch(`https://dev-online-gateway.ghn.vn/shiip/public-api/v2/shipping-order/create`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        token: 'b4e434ed-ae2f-11ee-8bfa-8a2dda8ec551',
        shop_id: 190756
      },
      body: JSON.stringify(data)
    })
      .then((response) => response.json())
      .then((data) => {
        const shipmentInfo = {
          fee: data.data.total_fee,
          shipment_date: data.data.expected_delivery_time,
          shipment_address: listAddress[currentAddress].id
        }
        
        dispath(setShipmentInfo(shipmentInfo))
        setShipfee(shipmentInfo.fee)
      })
      .catch((error) => console.error(error))
  }, [currentAddress])

  const handleCheckoutByVNPay = async () => {
    const order_id_for_vnpay = id + '_' + v4()
    const data = {
      order_id: order_id_for_vnpay,
      amount: subTotalPrice + 120000,
      order_desc: `Payment for order ${order_id_for_vnpay}`
    }
    try {
      let resultAction = await dispath(paymentVNPayMethod(data))
      let result = unwrapResult(resultAction)
      const urlPaymentGateway = result.data
      window.location.href = urlPaymentGateway
    } catch (e) {
      toast.error(<CustomToastContainer header='Error' message={e} />, {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: 'light'
      })
    }
  }

  const handleAddNewAddress = async (value) => {
    const data = {
      username: infoUser.username,
      home_number: value.home_number,
      commune: value.ward,
      state: value.district,
      city: value.city
    }
    try {
      let resultAction = await dispath(addShipmentAddressMethod(data))
      let result = unwrapResult(resultAction)
      setListAddress([
        ...listAddress,
        {
          id: result.data.id,
          address: result.data.home_number,
          ward: result.data.commune,
          district: result.data.state,
          province: result.data.city
        }
      ])
      toast.success(<CustomToastContainer header='Success' message='Add address successful' />, {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: 'light'
      })
    } catch (e) {
      toast.error(<CustomToastContainer header='Error' message={e} />, {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: 'light'
      })
    }
    setModalShow(false)
  }

  const handleCheckoutByZaloPay = async () => {
    const order_id_for_zalopay = id + '_' + v4()
    const data = {
      appuser: username,
      order_id: order_id_for_zalopay,
      amount: subTotalPrice + 120000,
      description: `Payment for order ${order_id_for_zalopay}`
    }
    try {
      let resultAction = await dispath(paymentZaloPayMethod(data))
      let result = unwrapResult(resultAction)
      if (result.code !== 201) {
        toast.error(<CustomToastContainer header='Error' message='payment by zalopay failed' />, {
          position: 'top-right',
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
          theme: 'light'
        })
        return
      }
      const urlPaymentGateway = result.data.createOrderResponse.orderurl
      window.location.href = urlPaymentGateway
    } catch (e) {
      toast.error(<CustomToastContainer header='Error' message={e} />, {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: 'light'
      })
    }
  }

  const handlePaymentAfterRecieve = () => {
    console.log('oke')
  }
  return (
    <>
      <Meta title='Checkout' />
      <SectionWrappep className='checkout-wrapper home-wrapper-2'>
        <ToastContainer />
        <div className='col-7'>
          <div className='checkout-left-data'>
            <h3 className='website-name'>T Store</h3>
            <nav aria-label='breadcrumb'>
              <ol className='breadcrumb'>
                <li className='breadcrumb-item'>
                  <Link to='/cart' className='text-dark'>
                    Cart
                  </Link>
                </li>
                <li className='breadcrumb-item active'>Information</li>
                <li className='breadcrumb-item active' aria-current='page'>
                  Shipping
                </li>
                <li className='breadcrumb-item active' aria-current='page'>
                  Payment
                </li>
              </ol>
            </nav>
            <h4 className='title fs-4'>Contact Information</h4>
            <p className='user-details'>
              <div>
                Name:{' '}
                <span className='fw-bold'>
                  {infoUser?.first_name} {infoUser?.last_name}
                </span>
              </div>
              <div>
                Phone Number:{' '}
                <span className='fw-bold'>
                  {infoUser?.telephoneNumber ? infoUser?.telephoneNumber : 'Not provided'}
                </span>
              </div>
            </p>
            <h4 className='title fs-4'>Shipping Address</h4>
            <div>
              {
                listAddress.length > 0 &&
                listAddress.map((item, index) => (
                  <div className='form-check mb-4' key={index}>
                    <input
                      className='form-check-input'
                      type='radio'
                      name='flexRadioDefault'
                      id={`flexRadioDefault-${index}`}
                      checked={index === currentAddress}
                      onChange={() => setCurrentAddress(index)}
                    />
                    <label className='form-check-label' htmlFor={`flexRadioDefault-${index}`}>
                      {item.address}, {item.ward}, {item.district}, {item.province}{' '}
                      {item.defaultAddress && <b>(default)</b>}
                    </label>
                  </div>
                ))
              }
              <button
                type='button'
                className='btn btn-outline-primary'
                onClick={() => {
                  setModalShow(true)
                }}
              >
                Add new address
              </button>

              <ModalAddAddress
                modalShow={modalShow}
                setModalShow={setModalShow}
                handleAddNewAddress={handleAddNewAddress}
              />
            </div>
            <div className='w-100'>
              <div className='d-flex justify-content-between align-items-center'>
                <Link to='/cart'>
                  <IoMdArrowBack /> Return to Cart
                </Link>
                <Link to='/store' className='button'>
                  Continue to Shopping
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className='col-5'>
          <div className='border-bottom py-4'>
            {listOrderItem && listOrderItem.map((item, index) => <CheckoutItem key={index} data={item} />)}
          </div>
          <div className='pt-4'>
            <div className='d-flex justify-content-between align-items-center'>
              <p className='total'>Subtotal</p>
              <p className='total-price'>{convertMoneyFormat(subTotalPrice)} VND</p>
            </div>
            <div className='d-flex justify-content-between align-items-center border-bottom'>
              <p className='total'>Shipping</p>
              <p className='total-price'>{convertMoneyFormat(shipfee)} VND</p>
            </div>
            <div className='d-flex justify-content-between align-items-center py-4'>
              <h4 className='total'>Total</h4>
              <h5 className='total-price'>{convertMoneyFormat(subTotalPrice + shipfee)} VND</h5>
            </div>
          </div>
          <div className='pt-4'>
            <h4 className='title fs-4'>Payment Information</h4>
            <div className='pt-4'>
              <Tab.Container id='list-group-tabs-example' defaultActiveKey=''>
                <Row>
                  <Col sm={4}>
                    <ListGroup>
                      <ListGroup.Item action href='#checkoutafter' onClick={handlePaymentAfterRecieve}>
                        Pay after recieve
                      </ListGroup.Item>
                      <ListGroup.Item action href='#checkoutonline'>
                        Online payment
                      </ListGroup.Item>
                    </ListGroup>
                  </Col>
                  <Col sm={8} className='d-flex align-items-center justify-content-center'>
                    <Tab.Content>
                      <Tab.Pane eventKey='#checkoutafter'>
                        <div className='text-center'>
                          <button className='button' onClick={handlePaymentAfterRecieve}>
                            Checkout
                          </button>
                        </div>
                      </Tab.Pane>
                      <Tab.Pane eventKey='#checkoutonline'>
                        <div className='text-center d-flex gap-10 justify-content-center'>
                          <button
                            className='button d-flex align-items-center justify-content-between p-2 gap-10'
                            onClick={handleCheckoutByVNPay}
                          >
                            <img
                              src='https://images02.vietnamworks.com//companyprofile/null/en/Logo_VNPAY.jpg'
                              alt=''
                              className='img-fluid'
                            />
                            <span className='fw-bold'>VN Pay</span>
                          </button>

                          <button
                            className='button d-flex align-items-center justify-content-between p-2 gap-10'
                            onClick={handleCheckoutByZaloPay}
                          >
                            <img
                              src='https://cdn.haitrieu.com/wp-content/uploads/2022/10/Logo-ZaloPay-Square.png'
                              alt=''
                              className='img-fluid'
                            />
                            <span className='fw-bold'>Zalo Pay</span>
                          </button>
                        </div>
                      </Tab.Pane>
                    </Tab.Content>
                  </Col>
                </Row>
              </Tab.Container>
            </div>
          </div>
        </div>
      </SectionWrappep>
    </>
  )
}

export default Checkout
