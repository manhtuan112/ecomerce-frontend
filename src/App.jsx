import { useState } from 'react'
import { Routes, Route } from 'react-router-dom'
import MainLayout from './layout'
import Home from './pages/home'
import Contact from './pages/contact'
import './App.scss'
import Store from './pages/store'
import Blog from './pages/blog'
import CompareProduct from './pages/compare_product'
import Wishlist from './pages/wishlist'
import Login from './pages/login'
import ForgetPassword from './pages/forget_password'
import Signup from './pages/signup'
import SingleBlog from './pages/single_blog'
import PrivacyPolicy from './pages/policy/PrivacyPolicy'
import RefundPolicy from './pages/policy/RefundPolicy'
import ShippingPolicy from './pages/policy/ShippingPolicy'
import TermAndConditions from './pages/policy/TermAndConditions'
import SingleProduct from './pages/single_product'
import Cart from './pages/cart'
import Checkout from './pages/checkout'
import NotFound from './pages/404_not_found'
import Profile from './pages/profile'
import PrivateRoute from './router/PrivateRoute'
import OpenRoute from './router/OpenRoute'
import PaymentSuccess from './pages/payment_success'

function App() {
  return (
    <>
      <Routes>
        <Route path='/' element={<MainLayout />}>
          <Route index element={<Home />} />
          <Route path='store' element={<Store />} />
          <Route path='product/:id' element={<SingleProduct />} />
          <Route path='blogs' element={<Blog />} />
          <Route path='blog/:id' element={<SingleBlog />} />
          <Route
            path='cart'
            element={
              <PrivateRoute>
                <Cart />
              </PrivateRoute>
            }
          />
          <Route
            path='checkout/:id'
            element={
              <PrivateRoute>
                <Checkout />
              </PrivateRoute>
            }
          />
          <Route path='contact' element={<Contact />} />
          <Route path='compare-product' element={<CompareProduct />} />
          <Route
            path='wishlist'
            element={
              <PrivateRoute>
                <Wishlist />
              </PrivateRoute>
            }
          />
          <Route
            path='login'
            element={
              <OpenRoute>
                <Login />
              </OpenRoute>
            }
          />
          <Route
            path='signup'
            element={
              <OpenRoute>
                <Signup />
              </OpenRoute>
            }
          />
          <Route path='user-information' element={<Profile />} />
          <Route path='forget-password' element={<ForgetPassword />} />
          <Route path='privacy-policy' element={<PrivacyPolicy />} />
          <Route path='refund-policy' element={<RefundPolicy />} />
          <Route path='shipping-policy' element={<ShippingPolicy />} />
          <Route path='term-conditions' element={<TermAndConditions />} />
          <Route path='payment/success/*' element={<PaymentSuccess />} />
          <Route path='*' element={<NotFound />} />
        </Route>
      </Routes>
    </>
  )
}

export default App
